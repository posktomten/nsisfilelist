;Created by nsisFileList 1.2.13
;Sat Jun 1 00:15:14 2024
;Copyright (C) 2022 - 2024 Ingemar Ceicer
;License GNU General Public License v3.0
;https://gitlab.com/posktomten/nsisfilelist


;Add/Install Files
SetOutPath "$INSTDIR"
file "Qt5Gui.dll"
file "Qt5Network.dll"
file "Qt5Svg.dll"
file "Qt5Widgets.dll"
file "unicon.ico"
file "icon.ico"
file "libcrypto-3.dll"
file "libgcc_s_dw2-1.dll"
file "libssl-3.dll"
file "libstdc++-6.dll"
file "libwinpthread-1.dll"
file "nsisfilelist.exe"
file "Qt5Core.dll"
file "msvcr100.dll"

SetOutPath "$INSTDIR\bearer"
file "bearer\qgenericbearer.dll"

SetOutPath "$INSTDIR\iconengines"
file "iconengines\qsvgicon.dll"

SetOutPath "$INSTDIR\imageformats"
file "imageformats\qgif.dll"
file "imageformats\qicns.dll"
file "imageformats\qico.dll"
file "imageformats\qjpeg.dll"
file "imageformats\qsvg.dll"
file "imageformats\qtga.dll"
file "imageformats\qtiff.dll"
file "imageformats\qwbmp.dll"
file "imageformats\qwebp.dll"

SetOutPath "$INSTDIR\License"
file "License\LICENSE.txt"

SetOutPath "$INSTDIR\platforms"
file "platforms\qwindows.dll"

SetOutPath "$INSTDIR\styles"
file "styles\qwindowsvistastyle.dll"



