;Delete files
Delete "$INSTDIR\*.*"
Delete "$INSTDIR\bearer\*.*"
Delete "$INSTDIR\iconengines\*.*"
Delete "$INSTDIR\imageformats\*.*"
Delete "$INSTDIR\License\*.*"
Delete "$INSTDIR\platforms\*.*"
Delete "$INSTDIR\styles\*.*"

;Remove installation folders
RMDir "$INSTDIR\bearer"
RMDir "$INSTDIR\iconengines"
RMDir "$INSTDIR\imageformats"
RMDir "$INSTDIR\License"
RMDir "$INSTDIR\platforms"
RMDir "$INSTDIR\styles"
RMDir "$INSTDIR"
