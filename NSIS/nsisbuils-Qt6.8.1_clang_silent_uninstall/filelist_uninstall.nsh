;Delete files
Delete "$INSTDIR\*.*"
Delete "$INSTDIR\platforms\*.*"
Delete "$INSTDIR\styles\*.*"
Delete "$INSTDIR\tls\*.*"
Delete "$INSTDIR\generic\*.*"
Delete "$INSTDIR\iconengines\*.*"
Delete "$INSTDIR\imageformats\*.*"
Delete "$INSTDIR\License\*.*"
Delete "$INSTDIR\networkinformation\*.*"

;Remove installation folders
RMDir "$INSTDIR\platforms"
RMDir "$INSTDIR\styles"
RMDir "$INSTDIR\tls"
RMDir "$INSTDIR\generic"
RMDir "$INSTDIR\iconengines"
RMDir "$INSTDIR\imageformats"
RMDir "$INSTDIR\License"
RMDir "$INSTDIR\networkinformation"
RMDir "$INSTDIR"
