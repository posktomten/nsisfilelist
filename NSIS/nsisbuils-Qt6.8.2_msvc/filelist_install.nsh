;Skapad av nsisFileList 1.3.0
;Sun Feb 9 15:48:57 2025
;Copyright (C) 2022 - 2025 Ingemar Ceicer
;Licens GNU General Public License v3.0
;https://gitlab.com/posktomten/nsisfilelist


;Add/Install Files
SetOutPath "$INSTDIR"
file "d3dcompiler_47.dll"
file "dxcompiler.dll"
file "dxil.dll"
file "icon.ico"
file "nsisfilelist.exe"
file "opengl32sw.dll"
file "Qt6Core.dll"
file "Qt6Gui.dll"
file "Qt6Network.dll"
file "Qt6Svg.dll"
file "Qt6Widgets.dll"

SetOutPath "$INSTDIR\styles"
file "styles\qmodernwindowsstyle.dll"

SetOutPath "$INSTDIR\tls"
file "tls\qcertonlybackend.dll"
file "tls\qschannelbackend.dll"

SetOutPath "$INSTDIR\generic"
file "generic\qtuiotouchplugin.dll"

SetOutPath "$INSTDIR\iconengines"
file "iconengines\qsvgicon.dll"

SetOutPath "$INSTDIR\imageformats"
file "imageformats\qgif.dll"
file "imageformats\qicns.dll"
file "imageformats\qico.dll"
file "imageformats\qjpeg.dll"
file "imageformats\qsvg.dll"
file "imageformats\qtga.dll"
file "imageformats\qtiff.dll"
file "imageformats\qwbmp.dll"
file "imageformats\qwebp.dll"

SetOutPath "$INSTDIR\License"
file "License\LICENSE.txt"

SetOutPath "$INSTDIR\networkinformation"
file "networkinformation\qnetworklistmanager.dll"

SetOutPath "$INSTDIR\platforms"
file "platforms\qwindows.dll"
