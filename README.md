# Program for creating a list of files. 
# Files included in an [NSIS](https://nsis.sourceforge.io/Download) (Nullsoft Scriptable Install System) installer.
# Read about the program and download. [Wiki](https://gitlab.com/posktomten/nsisfilelist/-/wikis/home)

The program uses these libraries:<br>
- [download_install](https://gitlab.com/posktomten/download_offline_installer)<br>
- [libabout](https://gitlab.com/posktomten/libabout)<br>
- [libcheckforupdates](https://gitlab.com/posktomten/libcheckforupdates)<br>
- [libcreateshortcut, Windows](https://gitlab.com/posktomten/libcreateshortcut_windows)<br>

