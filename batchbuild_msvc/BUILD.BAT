call "C:\Program Files\Microsoft Visual Studio\2019\Community\VC\Auxiliary\Build\vcvarsall.bat" x86
set PROFILE="nsisfilelist"
set QTVERSION="5.15.15"
set QMAKE="C:/Qt/%QTVERSION%/msvc/bin/qmake.exe"

mkdir build
cd build

    %QMAKE% -project ../../code/%PROFILE%.pro 
	%QMAKE% "CONFIG+=release" ../../code/%PROFILE%.pro -o Makefile -spec win32-msvc "CONFIG+=qtquickcompiler" 
	set CL="/MP"
    nmake.exe 
	cd ../build-executable5
    C:/Qt/%QTVERSION%/msvc/bin/windeployqt.exe -no-translations nsisfilelist.exe

