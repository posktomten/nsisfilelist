cmake_minimum_required(VERSION 3.16)
project(
  nsisfilelist
  VERSION 1.0
  LANGUAGES CXX)
set(QT_MINIMUM_VERSION 6.4.0)
set(CMAKE_INCLUDE_CURRENT_DIR ON)
# set(STATIC_LIB true)



set(CMAKE_INCLUDE_CURRENT_DIR ON)
set(CMAKE_AUTOUIC ON)
set(CMAKE_VERBOSE_MAKEFILE ON)
set(CMAKE_AUTORCC ON)
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_AUTOMOC ON)

message("*** Compiler: ${CMAKE_CXX_COMPILER}")
message("*** Compiler version: ${CMAKE_CXX_COMPILER_VERSION}")
message("*** CMAKE_CXX_FLAGS_RELEASE: ${CMAKE_CXX_FLAGS_RELEASE}")
message("*** CMAKE_CXX_FLAGS_DEBUG: ${CMAKE_CXX_FLAGS_DEBUG}")

find_package(QT NAMES Qt6 REQUIRED COMPONENTS Core LinguistTools)
find_package(Qt${QT_VERSION_MAJOR} REQUIRED COMPONENTS Gui Network Widgets
                                                       LinguistTools)

message(
  "*** Qt version: ${QT_VERSION_MAJOR}.${QT_VERSION_MINOR}.${QT_VERSION_PATCH}")
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY
    ${CMAKE_CURRENT_SOURCE_DIR}/../build-executable6)

if ("${CMAKE_CXX_COMPILER_ID}" MATCHES "Clang")
  MESSAGE("*** Windows Clang ***")

      if(CMAKE_BUILD_TYPE STREQUAL "Debug")
        message("Qt6 WIN32 Debug")
        set(libabout ${CMAKE_CURRENT_SOURCE_DIR}/../lib6_clang/libaboutd.a)
        set(libcreateshortcut
            ${CMAKE_CURRENT_SOURCE_DIR}/../lib6_clang/libcreateshortcutd.a)
        set(libcheckupdate ${CMAKE_CURRENT_SOURCE_DIR}/../lib6_clang/libcheckupdated.a)
        set(libdownload_install
            ${CMAKE_CURRENT_SOURCE_DIR}/../lib6_clang/libdownload_installd.a)
      endif()
      if(CMAKE_BUILD_TYPE STREQUAL "Release")
        message("Qt6 WIN32 Release")
        set(libabout ${CMAKE_CURRENT_SOURCE_DIR}/../lib6_clang/libabout.a)
        set(libcreateshortcut
            ${CMAKE_CURRENT_SOURCE_DIR}/../lib6_clang/libcreateshortcut.a)
        set(libcheckupdate ${CMAKE_CURRENT_SOURCE_DIR}/../lib6_clang/libcheckupdate.a)
        set(libdownload_install
            ${CMAKE_CURRENT_SOURCE_DIR}/../lib6_clang/libdownload_install.a)
      endif()

endif()

# if(${QT_VERSION_MAJOR} GREATER_EQUAL 6)
#   if(WIN32 AND NOT MSVC)
#     if(CMAKE_BUILD_TYPE STREQUAL "Debug")
#       message("Qt6 WIN32 Debug")
#       set(libabout ${CMAKE_CURRENT_SOURCE_DIR}/../lib6/libaboutd.a)
#       set(libcreateshortcut
#           ${CMAKE_CURRENT_SOURCE_DIR}/../lib6/libcreateshortcutd.a)
#       set(libcheckupdate ${CMAKE_CURRENT_SOURCE_DIR}/../lib6/libcheckupdated.a)
#       set(libdownload_install
#           ${CMAKE_CURRENT_SOURCE_DIR}/../lib6/libdownload_installd.a)
#     endif()
#     if(CMAKE_BUILD_TYPE STREQUAL "Release")
#       message("Qt6 WIN32 Release")
#       set(libabout ${CMAKE_CURRENT_SOURCE_DIR}/../lib6/libabout.a)
#       set(libcreateshortcut
#           ${CMAKE_CURRENT_SOURCE_DIR}/../lib6/libcreateshortcut.a)
#       set(libcheckupdate ${CMAKE_CURRENT_SOURCE_DIR}/../lib6/libcheckupdate.a)
#       set(libdownload_install
#           ${CMAKE_CURRENT_SOURCE_DIR}/../lib6/libdownload_install.a)
#     endif()
  # elseif(MSVC)
  if(MSVC)
    if(CMAKE_BUILD_TYPE STREQUAL "Debug")
      message("Qt6 MSVC Debug")
      set(libabout ${CMAKE_CURRENT_SOURCE_DIR}/../lib6_msvc/aboutd.lib)
      set(libcreateshortcut
          ${CMAKE_CURRENT_SOURCE_DIR}/../lib6_msvc/createshortcutd.lib)
      set(libcheckupdate
          ${CMAKE_CURRENT_SOURCE_DIR}/../lib6_msvc/checkupdated.lib)
      set(libdownload_install
          ${CMAKE_CURRENT_SOURCE_DIR}/../lib6_msvc/download_installd.lib)
    endif()
    if(CMAKE_BUILD_TYPE STREQUAL "Release")
      message("Qt6 MSVC Release")
      set(libabout ${CMAKE_CURRENT_SOURCE_DIR}/../lib6_msvc/about.lib)
      set(libcreateshortcut
          ${CMAKE_CURRENT_SOURCE_DIR}/../lib6_msvc/createshortcut.lib)
      set(libcheckupdate
          ${CMAKE_CURRENT_SOURCE_DIR}/../lib6_msvc/checkupdate.lib)
      set(libdownload_install
          ${CMAKE_CURRENT_SOURCE_DIR}/../lib6_msvc/download_install.lib)
    endif()

endif()

set(TS_FILES
    ${CMAKE_CURRENT_SOURCE_DIR}/i18n/_nsisfilelist_sv_SE.ts
    ${CMAKE_CURRENT_SOURCE_DIR}/i18n/_nsisfilelist_it_IT.ts
    ${CMAKE_CURRENT_SOURCE_DIR}/i18n/_nsisfilelist_template_xx_XX.ts)

set(SOURCE check_for_updates.cpp createlist.cpp deletesettings.cpp dialog.cpp
           main.cpp save.cpp)

set(HEADER dialog.h)

set(UI dialog.ui)
set(RESOURCES resources.qrc)

set(APP_ICON_RESOURCE_WINDOWS "${CMAKE_CURRENT_SOURCE_DIR}/myapp.rc")

qt_add_executable(
  nsisfilelist
  WIN32
  ${SOURCE}
  ${HEADER}
  ${UI}
  ${RESOURCES}
  ${APP_ICON_RESOURCE_WINDOWS}
  ${TSFILES})

qt_add_lupdate(nsisfilelist TS_FILES ${TS_FILES})

target_link_libraries(nsisfilelist PRIVATE Qt::Core Qt::Gui Qt::Network
                                           Qt::Widgets)

target_link_libraries(
  nsisfilelist PRIVATE ${libabout} ${libcreateshortcut} ${libcheckupdate}
                       ${libdownload_install})

if((QT_VERSION_MAJOR GREATER 4))
  target_link_libraries(nsisfilelist PRIVATE Qt::Widgets)
endif()

target_include_directories(nsisfilelist PRIVATE ../include)

# install(TARGETS nsisfilelist BUNDLE DESTINATION . RUNTIME DESTINATION
# ${CMAKE_INSTALL_BINDIR} )

# qt_generate_deploy_app_script( TARGET nsisfilelist FILENAME_VARIABLE
# deploy_script NO_UNSUPPORTED_PLATFORM_ERROR ) install(SCRIPT ${deploy_script})
