// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          nsisFileList
//          Copyright (C) 2022 - 2024 Ingemar Ceicer
//          https://gitlab.com/posktomten/nsisfilelist
//          programming@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation version 3.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
#include "dialog.h"
#include "checkupdate.h"
#include <QDebug>
#include <QTimer>

void Dialog::checkForUpdates()
{
    const QString *updateinstructions =
        new QString(tr("Please click on \"Update\""));
    auto *cfu = new CheckUpdate;
    QIcon *icon = new QIcon(QStringLiteral(":/images/icon.png"));
    cfu->check(DISPLAY_NAME, VERSION, VERSION_PATH, *updateinstructions, icon);
    connect(cfu, &CheckUpdate::foundUpdate, [ cfu ](bool found) {
        cfu->deleteLater();
        // qDebug() << "CHECK " << found;
    });
}

void Dialog::checkForUpdatesOnStart()
{
    const QString *updateinstructions =
        new QString(tr("Please click on \"Update\""));
    auto *cfu = new CheckUpdate;
    QIcon *icon = new QIcon(QStringLiteral(":/images/icon.png"));
    cfu->checkOnStart(DISPLAY_NAME, VERSION, VERSION_PATH, *updateinstructions, icon);
    connect(cfu, &CheckUpdate::foundUpdate, [ cfu ](bool found) {
        cfu->deleteLater();
        // qDebug() << "CHECK ON START " << found;
    });
}
