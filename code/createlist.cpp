// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          nsisFileList
//          Copyright (C) 2022 - 2024 Ingemar Ceicer
//          https://gitlab.com/posktomten/nsisfilelist
//          programming@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation version 3.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#include <QMimeData>
#include <QTemporaryFile>
#include <QMessageBox>
#include <QSettings>
#include <QFileDialog>
#include <QStandardPaths>
#include <QDirIterator>
#include <QUuid>
#include <QListView>
#include <QTreeView>
#include "dialog.h"


// CREATE LIST
void Dialog::createList()
{
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                       EXECUTABLE_NAME);
    settings.beginGroup("Paths");
    QString openpath = settings.value("openpath", QStandardPaths::writableLocation(QStandardPaths::HomeLocation)).toString();
    settings.endGroup();
    settings.beginGroup("Geometry");
    QRect filedialoggeometry = settings.value("filedialoggeometry", QRect(x(), y(), 900, 600)).toRect();
    settings.endGroup();
    // QFileDialog *fileDialog = new QFileDialog(this);
    QFileDialog fileDialog;
    fileDialog.setGeometry(filedialoggeometry);
    fileDialog.setDirectory(openpath);
    fileDialog.setWindowTitle(tr("Select files in base directory"));
    fileDialog.setOption(QFileDialog::DontUseNativeDialog, true);
    fileDialog.setLabelText(QFileDialog::Accept, tr("Select Files"));
    fileDialog.setLabelText(QFileDialog::Reject, tr("Cancel"));
    fileDialog.setOption(QFileDialog::ReadOnly);
    fileDialog.setFileMode(QFileDialog::ExistingFiles);

    if(fileDialog.exec()) {
        QUuid uuid = QUuid::createUuid();
        tmpFile = new QTemporaryFile(QTemporaryFile::createNativeFile(uuid.toString(QUuid::Id128) + ".dat"));
        stream = new QTextStream(tmpFile);

        if(!tmpFile->open()) {
            QMessageBox msgBox;
            msgBox.setWindowFlags(Qt::WindowStaysOnTopHint | Qt::WindowSystemMenuHint);
            msgBox.setIcon(QMessageBox::Critical);
            msgBox.setWindowTitle(DISPLAY_NAME " " DISPLAY_VERSION);
            msgBox.setText(tr("ERROR 1: An unexpected error occurred.<br>Could not open a temporary file."));
            msgBox.addButton(tr("Ok"), QMessageBox::AcceptRole);
            msgBox.exec();
            return;
        }

//        stream = new QTextStream(tmpFile);
        QDateTime nu(QDateTime::currentDateTime());
        QString currenttime = nu.toString(Qt::ISODate);
        QStringList filenames = fileDialog.selectedFiles();
        QString copyright_year;

        if(COPYRIGHT_YEAR != QString::number(QDate::currentDate().year())) {
            copyright_year = COPYRIGHT_YEAR " - " + QString::number(QDate::currentDate().year());
        } else {
            copyright_year = COPYRIGHT_YEAR;
        }

        const QString createdby = tr("Created by");
//        const QString license = tr("License");
        *stream << ";" + createdby << " " << DISPLAY_NAME " " DISPLAY_VERSION << "\n";
        *stream << ";" + currenttime + "\n";
        *stream << ";Copyright (C) " + copyright_year + " Ingemar Ceicer\n";
        *stream << ";https://gitlab.com/posktomten/nsisfilelist\n";
        *stream << "\n\n";
        *stream << "\n\n";
        *stream << ";Add/Install Files\n";
        *stream << "SetOutPath \"$INSTDIR\"\n";
        openpath = fileDialog.selectedFiles().at(0).mid(0, fileDialog.selectedFiles().at(0).lastIndexOf("/"));
        settings.beginGroup("Paths");
        settings.setValue("openpath", openpath);
        settings.endGroup();

        foreach(QString insdir,  filenames) {
            int hittat = insdir.lastIndexOf("/");
            insdir = insdir.mid(hittat + 1);
            *stream   << "file \"" + insdir + "\"\n";
        }
    } else {
        QUuid uuid = QUuid::createUuid();
        tmpFile = new QTemporaryFile(QTemporaryFile::createNativeFile(uuid.toString(QUuid::Id128) + ".dat"));
        stream = new QTextStream(tmpFile);

        if(!tmpFile->open()) {
            QMessageBox msgBox;
            msgBox.setWindowFlags(Qt::WindowStaysOnTopHint | Qt::WindowSystemMenuHint);
            msgBox.setIcon(QMessageBox::Critical);
            msgBox.setWindowTitle(DISPLAY_NAME " " DISPLAY_VERSION);
            msgBox.setText(tr("ERROR 1: An unexpected error occurred.<br>Could not open a temporary file."));
            msgBox.addButton(tr("Ok"), QMessageBox::AcceptRole);
            msgBox.exec();
            return;
        }

        QDateTime nu(QDateTime::currentDateTime());
        QString currenttime = nu.toString(Qt::ISODate);
        QString copyright_year;

        if(COPYRIGHT_YEAR != QString::number(QDate::currentDate().year())) {
            copyright_year = COPYRIGHT_YEAR " - " + QString::number(QDate::currentDate().year());
        } else {
            copyright_year = COPYRIGHT_YEAR;
        }

        const QString createdby = tr("Created by");
        const QString license = tr("License");
        *stream << ";" + createdby << " " << DISPLAY_NAME " " DISPLAY_VERSION << "\n";
        *stream << ";" + currenttime + "\n";
        *stream << ";Copyright (C) " + copyright_year + " Ingemar Ceicer\n";
        *stream << ";" + license + " GNU General Public License v3.0\n";
        *stream << ";https://gitlab.com/posktomten/nsisfilelist\n";
        *stream << "\n\n";
        *stream << "\n\n";
        *stream << ";Add/Install Files\n";
        *stream << "SetOutPath \"$INSTDIR\"\n";
    }

    // QString foldername;
    // QStringList foldernames;
    settings.beginGroup("Geometry");
    QRect filedialoggeometry2 = settings.value("filedialoggeometry", QRect(x(), y(), 900, 600)).toRect();
    settings.endGroup();
    QFileDialog *filedialog =  new QFileDialog;
    filedialog->setWindowTitle(tr("Select folders. Files in the folders will be included."));
    filedialog->setLabelText(QFileDialog::Accept, tr("Select Folders"));
    filedialog->setLabelText(QFileDialog::Reject, tr("Cancel"));
    filedialog->setDirectory(openpath);
    filedialog->setOption(QFileDialog::DontUseNativeDialog, true);
    filedialog->setFileMode(QFileDialog::Directory);
    filedialog->setGeometry(filedialoggeometry2);
    QListView *listview = filedialog->findChild<QListView*>("listView");

    if(listview) {
        listview->setSelectionMode(QAbstractItemView::MultiSelection);
    }

    QTreeView *treeview = filedialog->findChild<QTreeView*>();

    if(treeview) {
        treeview->setSelectionMode(QAbstractItemView::MultiSelection);
    }

    // ui->txtBrowser->append("SetOutPath \"$INSTDIR\"");
    list  = new QStringList;

    if(filedialog->exec()) {
        *list  = filedialog->selectedFiles();
    }

    filesInFolder(list, openpath);
    *stream << "\n\n";
    *stream << ";Delete files\n";
    *stream << "Delete \"$INSTDIR\\*.*\"\n";

    foreach(QString s, *list) {
        QDir dir(s);
        QString folderpath = dir.absolutePath();
        QDirIterator itfile(folderpath, QDirIterator::Subdirectories | QDirIterator::NoIteratorFlags);

        while(itfile.hasNext()) {
            QString next = itfile.next();

            if((next.right(1) != ".") && (next.right(2) != "..")) {
                QFileInfo fi(next);

                if(fi.isDir()) {
                    *stream << "Delete \"$INSTDIR\\" + QDir::toNativeSeparators(next).mid(next.lastIndexOf("/") + 1) + "\\*.*\"\n";
                }
            }
        }

        *stream << "Delete \"$INSTDIR\\" + s.mid(s.lastIndexOf("/") + 1) + "\\*.*\"\n";
    }

    *stream << "\n";
    *stream << ";Remove installation folder\n";

    foreach(QString s, *list) {
        *stream << "RMDir \"$INSTDIR\\" + s.mid(s.lastIndexOf("/") + 1) + "\"\n";
    }

    *stream << "RMDir \"$INSTDIR\"\n";
    ui->pbSave->setEnabled(true);
    settings.beginGroup("Settings");
    bool openfiledialog = settings.value("openfiledialog").toBool();
    settings.endGroup();

    if(openfiledialog) {
        ui->pbSave->animateClick();
    }

    settings.beginGroup("Geometry");
    settings.setValue("filedialoggeometry", fileDialog.geometry());
    settings.endGroup();
}

// FILES IN FOLDER
bool Dialog::filesInFolder(QStringList  *list, QString  openpath)
{
    int size = openpath.size();

    foreach(QString str, *list) {
        *stream  << "\nSetOutPath \"$INSTDIR\\" + QDir::toNativeSeparators(str.mid(size + 1)) + "\"\n";
        QDirIterator dit(str, QDir::Files);

        while(dit.hasNext()) {
            QString s = dit.next();
            *stream << "file \"" + QDir::toNativeSeparators(s.mid(size + 1)) + "\"\n";
        }
    }

    return true;
}

void Dialog::dropEvent(QDropEvent * ev)
{
//    this->setWindowState(Qt::WindowActive);
//    this->setFocus();
    this->activateWindow();
    QUuid uuid = QUuid::createUuid();
    tmpFile = new QTemporaryFile(QTemporaryFile::createNativeFile(uuid.toString(QUuid::Id128) + ".dat"));
    stream = new QTextStream(tmpFile);

    if(!tmpFile->open()) {
        QMessageBox msgBox;
        msgBox.setWindowFlags(Qt::WindowStaysOnTopHint | Qt::WindowSystemMenuHint);
        msgBox.setIcon(QMessageBox::Critical);
        msgBox.setWindowTitle(DISPLAY_NAME " " DISPLAY_VERSION);
        msgBox.setText(tr("ERROR 2: An unexpected error occurred.<br>Could not open a temporary file."));
        msgBox.addButton(tr("Ok"), QMessageBox::AcceptRole);
        msgBox.exec();
        return;
    }

    QDateTime nu(QDateTime::currentDateTime());
    QString currenttime = nu.toString();
    const QMimeData* dt = ev->mimeData();

    if(!dt->hasUrls()) {
        QMessageBox msgBox;
        msgBox.setWindowFlags(Qt::WindowStaysOnTopHint | Qt::WindowSystemMenuHint);
        msgBox.setIcon(QMessageBox::Critical);
        msgBox.setWindowTitle(DISPLAY_NAME " " DISPLAY_VERSION);
        msgBox.setText(tr("Could not paste data to clipboard.<br>You must paste paths to folders and files.<br>(When you drag and drop folders and files,<br>the paths are pasted.)"));
        msgBox.addButton(tr("Ok"), QMessageBox::AcceptRole);
        msgBox.exec();
        return;
    }

    QList<QUrl> urls = dt->urls();
    QStringList folders;
    QStringList files;

    foreach(QUrl url,  urls) {
        QString lokalulr = url.toLocalFile();
        QFileInfo fi(lokalulr);

        if(fi.isDir()) {
            folders << lokalulr;
        } else if(fi.isFile()) {
            files << lokalulr;
        }
    }

    const QString createdby = tr("Created by");
    const QString license = tr("License");
    QString copyright_year;

    if(COPYRIGHT_YEAR != QString::number(QDate::currentDate().year())) {
        copyright_year = COPYRIGHT_YEAR " - " + QString::number(QDate::currentDate().year());
    } else {
        copyright_year = COPYRIGHT_YEAR;
    }

    *stream << ";" + createdby << " " << DISPLAY_NAME " " DISPLAY_VERSION << "\n";
    *stream << ";" + currenttime + "\n";
    *stream << ";Copyright (C) " + copyright_year + " Ingemar Ceicer\n";
    *stream << ";" + license + " GNU General Public License v3.0\n";
    *stream << ";https://gitlab.com/posktomten/nsisfilelist\n";
    *stream << "\n\n";
    *stream << ";Add/Install Files\n";
    *stream << "SetOutPath \"$INSTDIR\"\n";

    if(!files.isEmpty()) {
        foreach(QString fil, files) {
            fil = fil.remove(fil.mid(0, fil.lastIndexOf("/") + 1));
            *stream << "file \"" + fil + "\"\n";
        }
    }

    if(!folders.isEmpty()) {
        foreach(QString folder, folders) {
            QFile fil(folder);
            QFileInfo fi(fil);
            QString bort = fi.path();
            QDirIterator itfile(folder, QStringList() << "*.*", QDir::Files, QDirIterator::Subdirectories);
            folder = folder.remove(folder.mid(0, folder.lastIndexOf("/") + 1));
            *stream << "\n";
            *stream  << "SetOutPath \"$INSTDIR\\" + folder + "\"\n";

            while(itfile.hasNext()) {
                QString next = itfile.next();
                QString fil =  next;
                fil = fil.remove(bort + "/");
                fil.replace("/", "\\");
                *stream << "file \"" + fil + "\"\n";
            }
        }
    }

    *stream << "\n\n";
    *stream << ";Delete files\n";
    *stream << "Delete \"$INSTDIR\\*.*\"\n";

    foreach(QString folder, folders) {
        *stream << "Delete \"$INSTDIR\\" + folder.mid(folder.lastIndexOf("/") + 1) + "\\*.*\"\n";
    }

    *stream << "\n";
    *stream << ";Remove installation folders\n";

    foreach(QString s, folders) {
        *stream << "RMDir \"$INSTDIR\\" + s.mid(s.lastIndexOf("/") + 1) + "\"\n";
    }

    *stream << "RMDir \"$INSTDIR\"\n";
    QSettings *settings = new QSettings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME, EXECUTABLE_NAME);
    settings->beginGroup("Settings");
    bool pastetoclipboard = settings->value("pastetoclipboard").toBool();
    settings->endGroup();

    if(pastetoclipboard) {
        pasteToClipboard();
    } else {
        ui->pbSave->setEnabled(true);
//        settings->beginGroup("Settings");
//        bool openfiledialog = settings->value("openfiledialog").toBool();
//        settings->endGroup();
//        if(openfiledialog) {
//            ui->pbSave->animateClick();
//        }
        ui->pbSave->animateClick();
        return;
    }
}

void Dialog::dragEnterEvent(QDragEnterEvent * ev)
{
    ev->accept();
}

void Dialog::pasteToClipboard()
{
    tmpFile->close();

//    connect(ui->pbSave, &QPushButton::clicked, [this]() {
    if(tmpFile->open()) {
        ;
        QClipboard *clipboard = QGuiApplication::clipboard();
        clipboard->setText(tmpFile->readAll());
        tmpFile->deleteLater();
        QMessageBox msgBox;
        msgBox.setWindowFlags(Qt::WindowStaysOnTopHint | Qt::WindowSystemMenuHint);
        msgBox.setIcon(QMessageBox::Information);
        msgBox.setWindowTitle(DISPLAY_NAME " " DISPLAY_VERSION);
        msgBox.setText(tr("Successfully pasted to clipboard."));
        msgBox.addButton(tr("Ok"), QMessageBox::AcceptRole);
        msgBox.exec();
        ui->pbSave->setEnabled(false);
    } else {
        QMessageBox msgBox;
        msgBox.setWindowFlags(Qt::WindowStaysOnTopHint | Qt::WindowSystemMenuHint);
        msgBox.setIcon(QMessageBox::Critical);
        msgBox.setWindowTitle(DISPLAY_NAME " " DISPLAY_VERSION);
        msgBox.setText(tr("Failed pasted to clipboard."));
        msgBox.addButton(tr("Ok"), QMessageBox::AcceptRole);
        msgBox.exec();
        return;
    }

//    });
}
