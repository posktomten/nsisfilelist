// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          nsisFileList
//          Copyright (C) 2022 - 2024 Ingemar Ceicer
//          https://gitlab.com/posktomten/nsisfilelist
//          programming@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation version 3.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#include <QFileInfo>
#include <QDir>
#include <QSettings>
#include <QMessageBox>
#include "dialog.h"


bool Dialog::deleteSettings(const QString &instructions)
{
    // Delete chortcuts, if anny
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                       EXECUTABLE_NAME);
    const QString settingsFile = settings.fileName();
    QFile file(settingsFile);
    QFileInfo fi(file);
    QDir dir(fi.filesystemAbsolutePath());
    QMessageBox msgBox;
    msgBox.setWindowFlags(Qt::WindowStaysOnTopHint | Qt::WindowSystemMenuHint);
    msgBox.setIcon(QMessageBox::Warning);
    msgBox.setWindowTitle(DISPLAY_NAME " " DISPLAY_VERSION);
    msgBox.setText(instructions);
    msgBox.addButton(tr("Yes"), QMessageBox::AcceptRole);
    QPushButton *but = msgBox.addButton(tr("No"), QMessageBox::RejectRole);
    msgBox.setDefaultButton(but);

    if(msgBox.exec() == QMessageBox::AcceptRole) {
        savesettings = false;

        if(!dir.removeRecursively()) {
            QMessageBox msgBox1;
            msgBox1.setIcon(QMessageBox::Critical);
            msgBox1.setWindowTitle(DISPLAY_NAME " " DISPLAY_VERSION);
            msgBox1.setText(tr("Failed to delete the program's settings files.<br>"
                               "Check your file permissions."));
            msgBox1.addButton(tr("Ok"), QMessageBox::AcceptRole);
            msgBox1.exec();
            savesettings = true;
            return false;
        }

        return true;
    } else {
        savesettings = true;
        return false;
    }
}

