
// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          nsisFileList
//          Copyright (C) 2022 - 2024 Ingemar Ceicer
//          https://gitlab.com/posktomten/nsisfilelist
//          programming@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation version 3.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#include <QFileDialog>
#include <QMenu>
#include <QProcess>
#include <QDesktopServices>
#include <QSettings>
#include <QStandardPaths>
#include <QPixmap>
#include <QDate>
#include <QStyle>
#include <QThread>
#include "dialog.h"
#include "ui_dialog.h"
#include "createshortcut_win.h"
#include "about.h"
#ifdef OFFLINE_INSTALLER
#include "download_install.h"
#endif
Dialog::Dialog(QDialog *parent)
    : QDialog(parent)
    , ui(new Ui::Dialog)
{
//    SetForegroundWindow((HWND)winId());
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME, EXECUTABLE_NAME);
    ui->setupUi(this);
    // qDebug() << "QApplication::style() " << QApplication::style();
    settings.beginGroup("Geometry");
    this->restoreGeometry(settings.value("savegeometry").toByteArray());
    settings.endGroup();
    QWidget::setAcceptDrops(true);
    savesettings = true;
    settings.beginGroup("Settings");
    bool pastetoclipboard = settings.value("pastetoclipboard").toBool();
    settings.endGroup();

    if(pastetoclipboard) {
        ui->pbSave->setText(tr("Paste to Clipboard"));
        ui->pbSave->setIcon(QIcon(QStringLiteral(":/images/clipboard.png")));
    } else {
        ui->pbSave->setText(tr("Save File list"));
        ui->pbSave->setIcon(QIcon(QStringLiteral(":/images/save.png")));
    }

    ui->lblPixmap->setPixmap(QPixmap(QStringLiteral(":/images/rightclick.png")));
    this->setWindowTitle(DISPLAY_NAME);
    this->setWindowIcon(QIcon(QStringLiteral(":/images/icon.png")));
    // CONTEXT MENU
    this->setContextMenuPolicy(Qt::CustomContextMenu);
    QObject::connect(this, &QDialog::customContextMenuRequested, this, [this]() {
        QSettings *settings = new QSettings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME, EXECUTABLE_NAME);
        QMenu *contextMenu = new QMenu;
        QAction *actionAbout = new QAction;
        actionAbout->setIcon(QIcon(QStringLiteral(":/images/about.png")));
        actionAbout->setText(tr("About"));
        contextMenu->addAction(actionAbout);
        QAction *actionAboutQt = new QAction;
        actionAboutQt->setIcon(QIcon(QStringLiteral(":/images/qt256.png")));
        actionAboutQt->setText(tr("About Qt"));
        contextMenu->addAction(actionAboutQt);
        contextMenu->addSeparator();
        QAction *actionPasteToClipboard = new QAction;
        contextMenu->addAction(actionPasteToClipboard);
        actionPasteToClipboard->setCheckable(true);
        actionPasteToClipboard->setText(tr("Paste to Clipboard (Do not save to file)"));
        settings->beginGroup("Settings");
        bool pastetoclipboard = settings->value("pastetoclipboard", false).toBool();
        bool openfiledialog = settings->value("openfiledialog", false).toBool();
        bool staysontophint = settings->value("staysontophint", true).toBool();
        settings->endGroup();

        if(pastetoclipboard) {
            actionPasteToClipboard->setChecked(true);
            ui->pbSave->setText(tr("Save File list"));
            ui->pbSave->setIcon(QIcon(QStringLiteral(":/images/save.png")));
        } else {
            actionPasteToClipboard->setChecked(false);
            ui->pbSave->setText(tr("Save File list"));
            ui->pbSave->setIcon(QIcon(QStringLiteral(":/images/clipboard.png")));
        }

        QAction *actionOpenFileDialog = new QAction;
        actionOpenFileDialog->setCheckable(true);
        actionOpenFileDialog->setText(tr("Open \"Save File list\" automatically"));
        contextMenu->addAction(actionOpenFileDialog);

        if(openfiledialog) {
            actionOpenFileDialog->setChecked(true);
        } else {
            actionOpenFileDialog->setChecked(false);
        }

        QAction *actionStaysOnTopHint = new QAction;
        actionStaysOnTopHint->setCheckable(true);
        actionStaysOnTopHint->setText(tr("Window always on top"));
        contextMenu->addAction(actionStaysOnTopHint);

        if(staysontophint) {
            actionStaysOnTopHint->setChecked(true);
        } else {
            actionStaysOnTopHint->setChecked(false);
        }

        contextMenu->addSeparator();
        QAction *actionEnglish = new QAction;
        actionEnglish->setIcon(QIcon(QStringLiteral(":/images/english.png")));
        actionEnglish->setText(tr("English"));
        contextMenu->addAction(actionEnglish);
        QAction *actionItalian = new QAction;
        actionItalian->setIcon(QIcon(QStringLiteral(":/images/italian.png")));
        actionItalian->setText(tr("Italian"));
        contextMenu->addAction(actionItalian);
        QAction *actionSwedish = new QAction;
        actionSwedish->setIcon(QIcon((":/images/swedish.png")));
        actionSwedish->setText(tr("Swedish"));
        contextMenu->addAction(actionSwedish);
        QAction *actionCustom = new QAction;
        actionCustom->setIcon(QIcon(QStringLiteral(":/images/language.png")));
        actionCustom->setText(tr("Select language file..."));
        contextMenu->addAction(actionCustom);
        QAction *actionChechUpdate = new QAction;
        actionChechUpdate->setText(tr("Check for updates"));
        actionChechUpdate->setIcon(QIcon(QStringLiteral(":/images/update.png")));
        contextMenu->addSeparator();
        contextMenu->addAction(actionChechUpdate);
        QAction *actionChechUpdateAtStart = new QAction;
        actionChechUpdateAtStart ->setText(tr("Check for updates at startup"));
        actionChechUpdateAtStart->setCheckable(true);
        settings->beginGroup("Updates");
        bool chechupdateatstart = settings->value("chechupdateatstart", true).toBool();
        settings->endGroup();

        if(chechupdateatstart) {
            actionChechUpdateAtStart->setChecked(true);
        } else {
            actionChechUpdateAtStart->setChecked(false);
        }

        contextMenu->addAction(actionChechUpdateAtStart);
        QAction *actionUpdate = new QAction;
        actionUpdate->setIcon(QIcon(QStringLiteral(":/images/update.png")));
        actionUpdate->setText(tr("Update"));
        contextMenu->addAction(actionUpdate);
        contextMenu->addSeparator();
        QAction *actionDeleteSettings = new QAction;
        actionDeleteSettings ->setText(tr("Delete all settings and exit"));
        actionDeleteSettings->setIcon(QIcon(QStringLiteral(":/images/uninstall1.png")));
        contextMenu->addAction(actionDeleteSettings);
#ifdef OFFLINE_INSTALLER
        QAction *actionUninstall = new QAction;
        actionUninstall ->setText(tr("Uninstall"));
        actionUninstall->setIcon(QIcon(QStringLiteral(":/images/uninstall1.png")));
        contextMenu->addAction(actionUninstall);
#endif
        contextMenu->addSeparator();
        QAction *actionDesktopShortcut = new QAction;
        actionDesktopShortcut->setCheckable(true);
        actionDesktopShortcut ->setText(tr("Desktop Shortcut"));
        settings->beginGroup("Shortcuts");
        bool desktopshortcut = settings->value("desktopshortcut").toBool();
        settings->endGroup();
        contextMenu->addSeparator();

        if(desktopshortcut) {
            actionDesktopShortcut->setChecked(true);
        } else {
            actionDesktopShortcut->setChecked(false);
        }

        contextMenu->addAction(actionDesktopShortcut);
#ifndef OFFLINE_INSTALLER
        QAction *actionApplicationShortcut = new QAction;
        actionApplicationShortcut->setCheckable(true);
        actionApplicationShortcut ->setText(tr("Applications menu Shortcut"));
        settings->beginGroup("Shortcuts");
        bool applicationmenushortcut = settings->value("applicationmenushortcut").toBool();
        settings->endGroup();

        if(applicationmenushortcut) {
            actionApplicationShortcut->setChecked(true);
        } else {
            actionApplicationShortcut->setChecked(false);
        }

        contextMenu->addAction(actionApplicationShortcut);
#endif
#if  QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
        contextMenu->addSeparator();
        QAction *actionLightTheme = new QAction(tr("Light Theme"));
        contextMenu->addAction(actionLightTheme);
        QAction *actionDarkTheme = new QAction(tr("Dark Theme"));
        contextMenu->addAction(actionDarkTheme);
        connect(actionLightTheme, &QAction::triggered, [this]() {
            QSettings settings(QSettings::IniFormat, QSettings::UserScope,
                               DISPLAY_NAME, EXECUTABLE_NAME);
            settings.beginGroup("Settings");
            settings.setValue("theme", "light");
            settings.endGroup();
            settings.sync();
            const QString EXECUTE =
                QCoreApplication::applicationDirPath() + "/" +
                QFileInfo(QCoreApplication::applicationFilePath()).fileName();
            QProcess p;
            p.setProgram(EXECUTE);
            close();
            p.startDetached();
        });

        connect(actionDarkTheme, &QAction::triggered, [this]() {
            QSettings settings(QSettings::IniFormat, QSettings::UserScope,
                               DISPLAY_NAME, EXECUTABLE_NAME);
            settings.beginGroup("Settings");
            settings.setValue("theme", "dark");
            settings.endGroup();
            settings.sync();
            const QString EXECUTE =
                QCoreApplication::applicationDirPath() + "/" +
                QFileInfo(QCoreApplication::applicationFilePath()).fileName();
            QProcess p;
#if QT_VERSION < QT_VERSION_CHECK(5, 6, 3)
            close();
            p.startDetached(EXECUTE);
#endif
#if QT_VERSION > QT_VERSION_CHECK(5, 6, 2)
            p.setProgram(EXECUTE);
            close();
            p.startDetached();
#endif
        });

#endif
        // ABOUT
        QObject::connect(actionAbout, &QAction::triggered, this, []() {
            const QPixmap *pixmap = new QPixmap(QStringLiteral(":/images/icon.png"));
//            QPixmap *pixmap = nullptr;
            const QString *purpose = new QString(tr("The program is intended for creating file lists for NSIS installer program."));
            const QString *translator = new QString(tr("Many thanks to bovirus for the Italian translation. And for many good ideas that have made the program better."));
//            const QString *translator = new QString("");
            /*

            */
            QSettings *settings = new QSettings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME, EXECUTABLE_NAME);
            settings->beginGroup("Settings");
            bool staysontophint = settings->value("staysontophint", true).toBool();
            settings->endGroup();
            QString copyright_year;

            if(COPYRIGHT_YEAR != QString::number(QDate::currentDate().year())) {
                copyright_year = COPYRIGHT_YEAR " - " + QString::number(QDate::currentDate().year());
            } else {
                copyright_year = COPYRIGHT_YEAR;
            }

            About *about = new About(QApplication::font(),
                                     DISPLAY_NAME,
                                     DISPLAY_VERSION,
                                     COPYRIGHT,
                                     EMAIL,
                                     &copyright_year,
                                     BUILD_DATE_TIME,
                                     LICENSE,
                                     LICENSE_LINK,
                                     CHANGELOG,
                                     SOURCECODE,
                                     WEBSITE,
                                     COMPILEDON,
                                     purpose,
                                     translator,
                                     pixmap,
                                     staysontophint);
            delete about;
            about = nullptr;
        });

        // ABOUTQT
        connect(actionAboutQt, &QAction::triggered, [this]() {
            QMessageBox::aboutQt(nullptr, DISPLAY_NAME " " DISPLAY_VERSION);
        });

        // SWEDISH
        QObject::connect(actionSwedish, &QAction::triggered, this, [this]() {
            QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                               EXECUTABLE_NAME);
            settings.beginGroup("Language");
            QString language = settings.value("language").toString();

            if(language == "sv_SE") {
                settings.endGroup();
                return;
            }

            settings.setValue("language", "sv_SE");
            settings.endGroup();
            settings.sync();
            const QString EXECUTE = QDir::toNativeSeparators(
                                        QCoreApplication::applicationDirPath() + "/" +
                                        QFileInfo(QCoreApplication::applicationFilePath()).fileName());
            settings.beginGroup("Geometry");
            settings.setValue("savegeometry", this->saveGeometry());
            settings.endGroup();
            settings.sync();
            QProcess p;
            p.setProgram(EXECUTE);
            p.setProgram(EXECUTE);
            close();
            p.startDetached();
        });

        // ENGLISH
        QObject::connect(actionEnglish, &QAction::triggered, this, [this]() {
            QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                               EXECUTABLE_NAME);
            settings.beginGroup("Language");
            QString language = settings.value("language").toString();

            if(language == "no_translation") {
                settings.endGroup();
                return;
            }

            settings.setValue("language", "no_translation");
            settings.endGroup();
            settings.sync();
            const QString EXECUTE = (QCoreApplication::applicationDirPath() + "/" +
                                     QFileInfo(QCoreApplication::applicationFilePath()).fileName());
            settings.beginGroup("Geometry");
            settings.setValue("savegeometry", this->saveGeometry());
            settings.endGroup();
            settings.sync();
            QProcess p;
            p.setProgram(EXECUTE);
            close();
            p.startDetached();
        });

        // ITALIAN
        QObject::connect(actionItalian, &QAction::triggered, this, [this]() {
            QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                               EXECUTABLE_NAME);
            settings.beginGroup("Language");
            QString language = settings.value("language").toString();

            if(language == "it_IT") {
                settings.endGroup();
                return;
            }

            settings.setValue("language", "it_IT");
            settings.endGroup();
            settings.sync();
            const QString EXECUTE = (QCoreApplication::applicationDirPath() + "/" +
                                     QFileInfo(QCoreApplication::applicationFilePath()).fileName());
            settings.beginGroup("Geometry");
            settings.setValue("savegeometry", this->saveGeometry());
            settings.endGroup();
            settings.sync();
            QProcess p;
            p.setProgram(EXECUTE);
            close();
            p.startDetached();
        });

        // CUSTOM
        QObject::connect(actionCustom, &QAction::triggered, this, [this]() {
            QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                               EXECUTABLE_NAME);
            settings.beginGroup("Language");
            QString customlanguagepath = settings.value("customlanguagepath", QStandardPaths::writableLocation(QStandardPaths::HomeLocation)).toString();
            settings.endGroup();
            settings.beginGroup("Geometry");
            QRect filedialoggeometry = settings.value("filedialoggeometry", QRect(x(), y(), 900, 600)).toRect();
            settings.endGroup();
            // QFILEDIALOG
            QFileDialog *dialog = new QFileDialog(this);
            dialog->setGeometry(filedialoggeometry);
            dialog->setLabelText(QFileDialog::Accept, tr("Open"));
            dialog->setLabelText(QFileDialog::Reject, tr("Cancel"));
            dialog->setWindowTitle(tr("Open language file"));
            dialog->setViewMode(QFileDialog::Detail);
            dialog->setFileMode(QFileDialog::AnyFile);
            dialog->setDirectory(customlanguagepath);
            dialog->setAcceptMode(QFileDialog::AcceptOpen);
            const QStringList filters({tr("Language files (*.qm)")});
            dialog->setNameFilters(filters);

            if(dialog->exec() == 1) {
                customlanguagepath = dialog->selectedFiles().at(0);
                // QFILEDIALOG END

                if(!customlanguagepath.isEmpty()) {
                    settings.beginGroup("Language");
                    settings.setValue("language", "custom");
                    settings.setValue("customlanguagepath", customlanguagepath);
                    settings.endGroup();
                    settings.sync();
                    const QString EXECUTE =
                        QCoreApplication::applicationDirPath() + "/" +
                        QFileInfo(QCoreApplication::applicationFilePath()).fileName();
                    settings.beginGroup("Geometry");
                    settings.setValue("savegeometry", this->saveGeometry());
                    settings.endGroup();
                    settings.sync();
                    QProcess p;
                    p.setProgram(EXECUTE);
                    close();
                    p.startDetached();
                }
            }

            settings.beginGroup("Geometry");
            settings.setValue("filedialoggeometry", dialog->geometry());
            settings.endGroup();;
        });

        // CHECK FOR UPDATES
        QObject::connect(actionChechUpdate, &QAction::triggered, this, [this]() {
            checkForUpdates();
        });

        // CHECK FOR UPDATES AT START
        QObject::connect(actionChechUpdateAtStart, &QAction::triggered, this, [actionChechUpdateAtStart]() {
            QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                               EXECUTABLE_NAME);
            settings.beginGroup("Updates");

            if(actionChechUpdateAtStart->isChecked()) {
                settings.setValue("chechupdateatstart", true);
            } else {
                settings.setValue("chechupdateatstart", false);
            }

            settings.endGroup();
        });

        // UPDATE
        QObject::connect(actionUpdate, &QAction::triggered, this, [ = ]() {
#ifdef OFFLINE_INSTALLER
            close();
            DownloadInstall *mDownloadInstall;
            mDownloadInstall = new DownloadInstall(nullptr);
            QString *path = new QString(QStringLiteral(PATH));
            QString *filename = new QString(QStringLiteral(FILENAME));
            QString *display_name = new QString(QStringLiteral(DISPLAY_NAME));
            QString *version = new QString(QStringLiteral(DISPLAY_VERSION));
            mDownloadInstall->setValues(path, filename, display_name, version);
            mDownloadInstall->show();
#endif
#ifndef OFFLINE_INSTALLER
            QDesktopServices::openUrl(QUrl(DOWNLOADS, QUrl::TolerantMode));
#endif
        });

        // PAST TO CLIPBOARD
        QObject::connect(actionPasteToClipboard, &QAction::triggered, this, [this, actionPasteToClipboard, actionOpenFileDialog]() {
            QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                               EXECUTABLE_NAME);
            settings.beginGroup("Settings");

            if(actionPasteToClipboard->isChecked()) {
                settings.setValue("pastetoclipboard", true);
                ui->pbSave->setText(tr("Paste to Clipboard"));
                ui->pbSave->setIcon(QIcon(QStringLiteral(":/images/clipboard.png")));
            } else {
                settings.setValue("pastetoclipboard", false);
                ui->pbSave->setText(tr("Save File list"));
                ui->pbSave->setIcon(QIcon(QStringLiteral(":/images/save.png")));
            }

            settings.endGroup();
        });

        // OPEN FILE DIALOG
        QObject::connect(actionOpenFileDialog, &QAction::triggered, this, [this, actionOpenFileDialog]() {
            QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                               EXECUTABLE_NAME);
            settings.beginGroup("Settings");

            if(actionOpenFileDialog->isChecked()) {
                settings.setValue("openfiledialog", true);
            } else {
                settings.setValue("openfiledialog", false);
            }

            settings.endGroup();
        });

        // STAYS ON TOP HINT
        QObject::connect(actionStaysOnTopHint, &QAction::triggered, this, [this, actionStaysOnTopHint]() {
            QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                               EXECUTABLE_NAME);
            settings.beginGroup("Settings");

            if(actionStaysOnTopHint->isChecked()) {
                settings.setValue("staysontophint", true);
            } else {
                settings.setValue("staysontophint", false);
            }

            settings.endGroup();
            settings.sync();
            const QString EXECUTE = (QCoreApplication::applicationDirPath() + "/" +
                                     QFileInfo(QCoreApplication::applicationFilePath()).fileName());
            settings.beginGroup("Geometry");
            settings.setValue("savegeometry", this->saveGeometry());
            settings.endGroup();
            settings.sync();
            QProcess p;
            p.setProgram(EXECUTE);
            close();
            p.startDetached();
        });

        // DELETE SETTINGS
        QObject::connect(actionDeleteSettings, &QAction::triggered, this, [this]() {
            if(deleteSettings(tr("All the program's settings files will be deleted.<br>Do you want to continue?"))) {
                close();
            }
        });

        // UNINSTALL
#ifdef OFFLINE_INSTALLER
        QObject::connect(actionUninstall, &QAction::triggered, this, [this]() {
            QSettings *settings = new QSettings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME, EXECUTABLE_NAME);
            settings->beginGroup("Shortcuts");
            settings->setValue("desktopshortcut", false);
            settings->endGroup();
            deleteSettings(tr("Do you want to remove all the program's<br>settings files before uninstalling?"));
            QString *executable_name = new QString(QStringLiteral(DISPLAY_NAME));
            Createshortcut::removeDesktopShortcutSilent(executable_name);
            const QString EXECUTE = QCoreApplication::applicationDirPath() + "/uninstall.exe";
            QProcess process;
            process.setProgram(EXECUTE);
            close();
            process.startDetached();
        });

#endif
        // DESKTOP SHORTCUT
        QObject::connect(actionDesktopShortcut, &QAction::triggered, this, [actionDesktopShortcut]() {
            QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                               EXECUTABLE_NAME);
            settings.beginGroup("Shortcuts");

            if(actionDesktopShortcut->isChecked()) {
                settings.setValue("desktopshortcut", true);
                QString *display_name = new QString(QStringLiteral(DISPLAY_NAME));
                QString *executable_name = new QString(QStringLiteral(EXECUTABLE_NAME));
                Createshortcut::makeShortcutFile(display_name, executable_name, false, true);
            } else {
                settings.setValue("desktopshortcut", false);
                QString *display_name = new QString(QStringLiteral(DISPLAY_NAME));
                Createshortcut::removeDesktopShortcut(display_name);
            }

            settings.endGroup();
        });

#ifndef OFFLINE_INSTALLER
        // APPLICATIONS MENU SHORTCUT
        QObject::connect(actionApplicationShortcut, &QAction::triggered, this, [actionApplicationShortcut]() {
            QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                               EXECUTABLE_NAME);
            settings.beginGroup("Shortcuts");

            if(actionApplicationShortcut->isChecked()) {
                settings.setValue("applicationmenushortcut", true);
                QString *display_name = new QString(QStringLiteral(DISPLAY_NAME));
                QString *executable_name = new QString(QStringLiteral(DISPLAY_NAME));
                Createshortcut::makeShortcutFile(display_name, executable_name, true, false);
            } else {
                settings.setValue("applicationmenushortcut", false);
                QString *executable_name = new QString(QStringLiteral(DISPLAY_NAME));
                Createshortcut::removeApplicationShortcut(executable_name);
            }
        });

#endif
        connect(contextMenu, &QMenu::aboutToShow, this, [this]() {
            QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                               EXECUTABLE_NAME);
            settings.beginGroup("Settings");
            bool pastetoclipboard = settings.value("pastetoclipboard").toBool();
            settings.endGroup();

            if(pastetoclipboard) {
                ui->pbSave->setText(tr("Paste to Clipboard"));
                ui->pbSave->setIcon(QIcon(QStringLiteral(":/images/clipboard.png")));
            } else {
                ui->pbSave->setText(tr("Save File list"));
                ui->pbSave->setIcon(QIcon(QStringLiteral(":/images/save.png")));
            }
        });

        connect(contextMenu, &QMenu::aboutToHide, this, [this]() {
            QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                               EXECUTABLE_NAME);
            settings.beginGroup("Settings");
            bool pastetoclipboard = settings.value("pastetoclipboard").toBool();
            settings.endGroup();

            if(pastetoclipboard) {
                ui->pbSave->setText(tr("Paste to Clipboard"));
                ui->pbSave->setIcon(QIcon(QStringLiteral(":/images/clipboard.png")));
            } else {
                ui->pbSave->setText(tr("Save File list"));
                ui->pbSave->setIcon(QIcon(QStringLiteral(":/images/save.png")));
            }
        });

        contextMenu->exec(QCursor::pos());
        contextMenu->clear();
        contextMenu->deleteLater();
    });

    // END CONTEXT MENU
    // CHECK FOR UPDATES ON START
    settings.beginGroup("Updates");
    bool chechupdateatstart = settings.value("chechupdateatstart", true).toBool();
    settings.endGroup();

    if(chechupdateatstart) {
        checkForUpdatesOnStart();
    }

    connect(ui->pbClose, &QPushButton::clicked, this, &QDialog::close);
    connect(ui->pbSave, &QPushButton::clicked, this, [this]() {
        QSettings *settings = new QSettings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME, EXECUTABLE_NAME);
        settings->beginGroup("Settings");
        bool pastetoclipboard = settings->value("pastetoclipboard").toBool();
        settings->endGroup();

        if(pastetoclipboard) {
            pasteToClipboard();
        } else {
            save();
        }
    });

    connect(ui->pbCreateList, &QPushButton::clicked, this, [this]() {
        createList();
    });

#ifdef OFFLINE_INSTALLER
    const QString tmppath = QStandardPaths::writableLocation(QStandardPaths::TempLocation);
    QFile fil(tmppath + "/" FILENAME);

    if(fil.exists()) {
        fil.remove();
    }

#endif
}

void Dialog::setEndConfig()
{
    if(!savesettings) {
        return;
    }

    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                       EXECUTABLE_NAME);
    settings.beginGroup("Geometry");
    settings.setValue("savegeometry", this->saveGeometry());
    settings.endGroup();
    settings.sync();
}

Dialog::~Dialog()
{
    delete ui;
}
