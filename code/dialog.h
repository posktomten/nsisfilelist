// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          nsisFileList
//          Copyright (C) 2022 - 2024 Ingemar Ceicer
//          https://gitlab.com/posktomten/nsisfilelist
//          programming@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation version 3.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#ifndef DIALOG_H
#define DIALOG_H

#include <QDebug>
#include <QFile>
#include <QDialog>
#include <QDragEnterEvent>
#include <QTemporaryFile>
#include <QMimeData>
#include <QClipboard>
#include <QColor>
#include <QPixmap>
#include "ui_dialog.h"
#include "checkupdate.h"
//EDIT
#define OFFLINE_INSTALLER

#define COPYRIGHT "Ingemar Ceicer"
#define COPYRIGHT_YEAR "2022"
#define EMAIL "programming@ceicer.com"
#define DISPLAY_NAME "nsisFileList"
#define EXECUTABLE_NAME "nsisfilelist"
#define VERSION "1.3.1"
#define DISPLAY_VERSION VERSION
#define BUILD_DATE_TIME __DATE__ " " __TIME__



#define DOWNLOADS "https://gitlab.com/posktomten/nsisfilelist/-/wikis/DOWNLOADS"

#define VERSION_PATH "https://bin.ceicer.com/nsisfilelist/version.txt"

#define SOURCECODE "https://gitlab.com/posktomten/nsisfilelist"
#define WEBSITE "https://gitlab.com/posktomten/nsisfilelist/-/wikis/home"
#define CHANGELOG "https://gitlab.com/posktomten/nsisfilelist/-/blob/master/CHANGELOG"
#define LICENSE "GNU General Public License v3.0"
#define LICENSE_LINK "https://gitlab.com/posktomten/nsisfilelist/-/blob/master/LICENSE"

#ifdef Q_PROCESSOR_X86_64
//#define COMPILEDON "Windows 10 Pro Version 22H2 x86_64<br>Build: 19045.3996"
#define COMPILEDON "Windows 11 Pro Version 23H2 x86_64<br>Build: 26100.2894"
#endif

#ifdef Q_PROCESSOR_X86_32
// #define COMPILEDON "Windows 10 Pro Version 22H2 i386<br>Build: 19045.3930"
#define COMPILEDON "Windows 11 Pro Version 23H2 x86_64<br>Build: 22631.4249"
#endif

#if QT_VERSION >= QT_VERSION_CHECK(6, 0, 0)
#define FONTSIZE 10
#endif

#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
#define FONTSIZE 10
#endif

// #ifdef Q_PROCESSOR_X86_64 // 64 bit
// #if _MSC_VER && !__INTEL_COMPILER // Compiler
// #define PATH "https://bin.ceicer.com/nsisfilelist/bin/windows/obsolete/"
// #define FILENAME "nsisFileList_64-bit_setup.exe"
// //#define WIN7
// #elif __MINGW32__
// #define PATH "https://bin.ceicer.com/nsisfilelist/bin/windows/"
// #define FILENAME "nsisFileList_64-bit_setup.exe"
// #endif // Compiler
// #endif // 64 bit

// #ifdef Q_PROCESSOR_X86_32 // 32 bit

// #define PATH "https://bin.ceicer.com/nsisfilelist/bin/windows/obsolete/"
// #define FILENAME "nsisFileList_32-bit_setup.exe"

// #endif // 32 bit


#define PATH "https://bin.ceicer.com/nsisfilelist/bin/windows/"
#define FILENAME "nsisFileList_64-bit_setup.exe"


QT_BEGIN_NAMESPACE

namespace Ui
{
class Dialog;
}

QT_END_NAMESPACE

class Dialog : public QDialog
{
    Q_OBJECT

public:
    Dialog(QDialog *parent = nullptr);
    ~Dialog();

    void setEndConfig();

protected:
    void dropEvent(QDropEvent *ev)  override;
    void dragEnterEvent(QDragEnterEvent *ev)  override;
    void pasteToClipboard();

private:
    Ui::Dialog *ui;

    QTemporaryFile *tmpFile;
    QTextStream *stream;
    bool filesInFolder(QStringList *list, QString openpath);

    void checkForUpdates();
    void checkForUpdatesOnStart();
    // CREATE LIST
    void createList();
    bool deleteSettings(const QString &instructions);
    bool savesettings;
    void save();
    QStringList *list;





};

#endif // DIALOG_H
