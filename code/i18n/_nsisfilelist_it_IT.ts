<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="it_IT">
<context>
    <name>Dialog</name>
    <message>
        <location filename="../dialog.ui" line="37"/>
        <source>New NSIS File list</source>
        <translation>Nuovo elenco file NSIS</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="88"/>
        <source>Right click here</source>
        <translation>Fai clic destro
mouse qui</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="59"/>
        <location filename="../dialog.cpp" line="93"/>
        <location filename="../dialog.cpp" line="97"/>
        <location filename="../dialog.cpp" line="526"/>
        <location filename="../dialog.cpp" line="693"/>
        <location filename="../dialog.cpp" line="709"/>
        <source>Save File list</source>
        <translation>Salva elenco file</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="77"/>
        <source>Exit</source>
        <translation>Esci</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="104"/>
        <source>x</source>
        <translation>x</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="272"/>
        <source>The program is intended for creating file lists for NSIS installer program.</source>
        <translation>Il programma è destinato alla creazione di elenchi di file per il programma di installazione di NSIS.</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="273"/>
        <source>Many thanks to bovirus for the Italian translation. And for many good ideas that have made the program better.</source>
        <translation>Molte grazie a bovirus per la traduzioen italiana. E per molte buone idee per rendere il programma migliore.</translation>
    </message>
    <message>
        <location filename="../check_for_updates.cpp" line="23"/>
        <location filename="../check_for_updates.cpp" line="34"/>
        <source>Please click on &quot;Update&quot;</source>
        <translation>Seleziona &quot;Aggiorna&quot;</translation>
    </message>
    <message>
        <location filename="../createlist.cpp" line="50"/>
        <source>Select files in base directory</source>
        <translation>Seleziona i file nella cartella base</translation>
    </message>
    <message>
        <location filename="../createlist.cpp" line="52"/>
        <source>Select Files</source>
        <translation>Seleziona i file</translation>
    </message>
    <message>
        <location filename="../createlist.cpp" line="53"/>
        <location filename="../createlist.cpp" line="153"/>
        <location filename="../dialog.cpp" line="430"/>
        <location filename="../save.cpp" line="48"/>
        <location filename="../save.cpp" line="65"/>
        <source>Cancel</source>
        <translation>Annulla</translation>
    </message>
    <message>
        <location filename="../createlist.cpp" line="68"/>
        <location filename="../createlist.cpp" line="116"/>
        <source>ERROR 1: An unexpected error occurred.&lt;br&gt;Could not open a temporary file.</source>
        <translation>ERRORE 1: si è verificato un errore imprevisto.&lt;br&gt;Impossibile aprire un file temporaneo.</translation>
    </message>
    <message>
        <location filename="../createlist.cpp" line="69"/>
        <location filename="../createlist.cpp" line="117"/>
        <location filename="../createlist.cpp" line="259"/>
        <location filename="../createlist.cpp" line="274"/>
        <location filename="../createlist.cpp" line="401"/>
        <location filename="../createlist.cpp" line="410"/>
        <location filename="../deletesettings.cpp" line="58"/>
        <location filename="../save.cpp" line="47"/>
        <location filename="../save.cpp" line="80"/>
        <location filename="../save.cpp" line="99"/>
        <location filename="../save.cpp" line="116"/>
        <source>Ok</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../createlist.cpp" line="86"/>
        <location filename="../createlist.cpp" line="132"/>
        <location filename="../createlist.cpp" line="294"/>
        <source>Created by</source>
        <translation>Creato da</translation>
    </message>
    <message>
        <location filename="../createlist.cpp" line="133"/>
        <location filename="../createlist.cpp" line="295"/>
        <source>License</source>
        <translation>Licenza</translation>
    </message>
    <message>
        <location filename="../createlist.cpp" line="151"/>
        <source>Select folders. Files in the folders will be included.</source>
        <translation>Seleziona le cartelle. I file nelle cartelle saranno inclusi.</translation>
    </message>
    <message>
        <source>Select directories in any place. (Files in subdirectories will also be included.)</source>
        <translation type="vanished">Seleziona le cartelle in qualsiasi percorso (saranno inclusi anche i file nelle sottocartelle)</translation>
    </message>
    <message>
        <location filename="../createlist.cpp" line="152"/>
        <source>Select Folders</source>
        <translation>Seleziona cartelle</translation>
    </message>
    <message>
        <location filename="../createlist.cpp" line="258"/>
        <source>ERROR 2: An unexpected error occurred.&lt;br&gt;Could not open a temporary file.</source>
        <translation>ERRORE 2: si è verificato un errore imprevisto.&lt;br&gt;Impossibile aprire un file temporaneo.</translation>
    </message>
    <message>
        <location filename="../createlist.cpp" line="273"/>
        <source>Could not paste data to clipboard.&lt;br&gt;You must paste paths to folders and files.&lt;br&gt;(When you drag and drop folders and files,&lt;br&gt;the paths are pasted.)</source>
        <translation>Impossibile incollare i dati negli Appunti.&lt;br&gt;Devi incollare i percorsi di cartelle e file.&lt;br&gt;(quando trascini e rilasci cartelle e file,&lt;br&gt;i percorsi vengono incollati)</translation>
    </message>
    <message>
        <location filename="../createlist.cpp" line="400"/>
        <source>Successfully pasted to clipboard.</source>
        <translation>Incollato correttamente negli Appunti.</translation>
    </message>
    <message>
        <location filename="../createlist.cpp" line="409"/>
        <source>Failed pasted to clipboard.</source>
        <translation>Impossibile incollare negli Appunti.</translation>
    </message>
    <message>
        <location filename="../deletesettings.cpp" line="45"/>
        <source>Yes</source>
        <translation>Sì</translation>
    </message>
    <message>
        <location filename="../deletesettings.cpp" line="46"/>
        <source>No</source>
        <translation>No</translation>
    </message>
    <message>
        <location filename="../deletesettings.cpp" line="56"/>
        <source>Failed to delete the program&apos;s settings files.&lt;br&gt;Check your file permissions.</source>
        <translation>Impossibile eliminare i file delle impostazioni del programma.&lt;br&gt;Controlla i tuoi permessi sui file.</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="56"/>
        <location filename="../dialog.cpp" line="521"/>
        <location filename="../dialog.cpp" line="690"/>
        <location filename="../dialog.cpp" line="706"/>
        <source>Paste to Clipboard</source>
        <translation>Incolla negli Appunti</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="74"/>
        <source>About</source>
        <translation>Info programma</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="78"/>
        <source>About Qt</source>
        <translation>Info Qt</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="84"/>
        <source>Paste to Clipboard (Do not save to file)</source>
        <translation>Incolla negli Appunti (non salvare nel file)</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="103"/>
        <source>Open &quot;Save File list&quot; automatically</source>
        <translation>Apri automaticamente &quot;Salva elenco file&quot;</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="114"/>
        <source>Window always on top</source>
        <translation>Finestra sempre in primo piano</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="126"/>
        <source>English</source>
        <translation>Inglese</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="130"/>
        <source>Italian</source>
        <translation>Italiano</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="134"/>
        <source>Swedish</source>
        <translation>Svedese</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="138"/>
        <source>Select language file...</source>
        <translation>Seleziona file lingua...</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="141"/>
        <source>Check for updates</source>
        <translation>Controlla aggiornamenti</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="146"/>
        <source>Check for updates at startup</source>
        <translation>Controlla aggiornamenti all&apos;avvio</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="161"/>
        <source>Update</source>
        <translation>Aggiorna</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="165"/>
        <source>Delete all settings and exit</source>
        <translation>Elimina tutte le impostazioni ed esci</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="170"/>
        <source>Uninstall</source>
        <translation>Disinstalla</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="177"/>
        <source>Desktop Shortcut</source>
        <translation>Collegamento sul desktop</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="204"/>
        <source>Applications menu Shortcut</source>
        <translation>Collegamento menu applicazioni</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="219"/>
        <source>Light Theme</source>
        <translation>Tema chiaro</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="221"/>
        <source>Dark Theme</source>
        <translation>Tema scuro</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="429"/>
        <source>Open</source>
        <translation>Apri</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="431"/>
        <source>Open language file</source>
        <translation>Apri file lingua</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="436"/>
        <source>Language files (*.qm)</source>
        <translation>File lingua (*.qm)</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="586"/>
        <source>All the program&apos;s settings files will be deleted.&lt;br&gt;Do you want to continue?</source>
        <translation>Tutti i file delle impostazioni del programma verranno eliminati.&lt;br&gt;Vuoi continuare?</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="598"/>
        <source>Do you want to remove all the program&apos;s&lt;br&gt;settings files before uninstalling?</source>
        <translation>Vuoi rimuovere tutti i file delle impostazioni del&lt;br&gt;programma prima della disinstallazione?</translation>
    </message>
    <message>
        <location filename="../save.cpp" line="45"/>
        <source>Save NSIS File list</source>
        <translation>Salva elenco file NSIS</translation>
    </message>
    <message>
        <location filename="../save.cpp" line="63"/>
        <source>The file exists.&lt;br&gt;Do you want to overwrite the old file with the new file?</source>
        <translation>Il file esiste.&lt;br&gt;Vuoi sovrascrivere il vecchio file con il nuovo file?</translation>
    </message>
    <message>
        <location filename="../save.cpp" line="64"/>
        <source>Overwrite</source>
        <translation>Sorascrivi</translation>
    </message>
    <message>
        <location filename="../save.cpp" line="79"/>
        <location filename="../save.cpp" line="98"/>
        <source>Could not save the file.&lt;br&gt;Check your file permissions.</source>
        <translation>Impossibile salvare il file.&lt;br&gt;Controlla i tuoi permessi sui file.</translation>
    </message>
    <message>
        <location filename="../save.cpp" line="115"/>
        <source>ERROR 3: An unexpected error occurred.&lt;br&gt;Could not save the file.</source>
        <translation>ERRORE 3: si è verificato un errore imprevisto.&lt;br&gt;Impossibile salvare il file.</translation>
    </message>
</context>
</TS>
