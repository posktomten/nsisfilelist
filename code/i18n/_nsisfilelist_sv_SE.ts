<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="sv_SE">
<context>
    <name>Dialog</name>
    <message>
        <location filename="../dialog.ui" line="37"/>
        <source>New NSIS File list</source>
        <translation>Ny NSIS fillista</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="88"/>
        <source>Right click here</source>
        <translation>Högerklicka här</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="59"/>
        <location filename="../dialog.cpp" line="93"/>
        <location filename="../dialog.cpp" line="97"/>
        <location filename="../dialog.cpp" line="526"/>
        <location filename="../dialog.cpp" line="693"/>
        <location filename="../dialog.cpp" line="709"/>
        <source>Save File list</source>
        <translation>Spara fillista</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="77"/>
        <source>Exit</source>
        <translation>Avsluta</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="104"/>
        <source>x</source>
        <translation>x</translation>
    </message>
    <message>
        <location filename="../createlist.cpp" line="50"/>
        <source>Select files in base directory</source>
        <translation>Välj filer i basfoldern</translation>
    </message>
    <message>
        <location filename="../createlist.cpp" line="52"/>
        <source>Select Files</source>
        <translation>Välj filer</translation>
    </message>
    <message>
        <location filename="../createlist.cpp" line="53"/>
        <location filename="../createlist.cpp" line="153"/>
        <location filename="../dialog.cpp" line="430"/>
        <location filename="../save.cpp" line="48"/>
        <location filename="../save.cpp" line="65"/>
        <source>Cancel</source>
        <translation>Avbryt</translation>
    </message>
    <message>
        <location filename="../createlist.cpp" line="68"/>
        <location filename="../createlist.cpp" line="116"/>
        <source>ERROR 1: An unexpected error occurred.&lt;br&gt;Could not open a temporary file.</source>
        <translation>FEL 1: Ett oväntat fel inträffade.&lt;br&gt;Kunde inte öppna en tillfällig fil.</translation>
    </message>
    <message>
        <location filename="../createlist.cpp" line="133"/>
        <location filename="../createlist.cpp" line="295"/>
        <source>License</source>
        <translation>Licens</translation>
    </message>
    <message>
        <location filename="../createlist.cpp" line="151"/>
        <source>Select folders. Files in the folders will be included.</source>
        <translation>Välj mappar. Filer i mapparna kommer att inkluderas.</translation>
    </message>
    <message>
        <location filename="../createlist.cpp" line="152"/>
        <source>Select Folders</source>
        <translation>Välj mappar</translation>
    </message>
    <message>
        <location filename="../createlist.cpp" line="258"/>
        <source>ERROR 2: An unexpected error occurred.&lt;br&gt;Could not open a temporary file.</source>
        <translation>FEL 2: Ett oväntat fel inträffade.&lt;br&gt;Kunde inte öppna en tillfällig fil.</translation>
    </message>
    <message>
        <location filename="../createlist.cpp" line="273"/>
        <source>Could not paste data to clipboard.&lt;br&gt;You must paste paths to folders and files.&lt;br&gt;(When you drag and drop folders and files,&lt;br&gt;the paths are pasted.)</source>
        <translation>Det gick inte att klistra in data i urklipp.&lt;br&gt;Du måste klistra in sökvägar till mappar och filer.&lt;br&gt;(När du drar och släpper mappar och filer&lt;br&gt;klistras sökvägarna in.)</translation>
    </message>
    <message>
        <location filename="../createlist.cpp" line="400"/>
        <source>Successfully pasted to clipboard.</source>
        <translation>Klistrades in i urklipp.</translation>
    </message>
    <message>
        <location filename="../createlist.cpp" line="409"/>
        <source>Failed pasted to clipboard.</source>
        <translation>Det gick inte att klistra in i urklipp.</translation>
    </message>
    <message>
        <location filename="../createlist.cpp" line="86"/>
        <location filename="../createlist.cpp" line="132"/>
        <location filename="../createlist.cpp" line="294"/>
        <source>Created by</source>
        <translation>Skapad av</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="74"/>
        <source>About</source>
        <translation>Om</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="84"/>
        <source>Paste to Clipboard (Do not save to file)</source>
        <translation>Klistra in i Urklipp (Spara inte till fil)</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="130"/>
        <source>Italian</source>
        <translation>Italienska</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="134"/>
        <source>Swedish</source>
        <translation>Svenska</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="126"/>
        <source>English</source>
        <translation>Engelska</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="56"/>
        <location filename="../dialog.cpp" line="521"/>
        <location filename="../dialog.cpp" line="690"/>
        <location filename="../dialog.cpp" line="706"/>
        <source>Paste to Clipboard</source>
        <translation>Klistra in i Urklipp</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="78"/>
        <source>About Qt</source>
        <translation>Om Qt</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="103"/>
        <source>Open &quot;Save File list&quot; automatically</source>
        <translation>Öppna &quot;Spara fillista&quot; automatiskt</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="114"/>
        <source>Window always on top</source>
        <translation>Fönstret alltid överst</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="138"/>
        <source>Select language file...</source>
        <translation>Välj språkfil...</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="141"/>
        <source>Check for updates</source>
        <translation>Sök efter uppdateringar</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="146"/>
        <source>Check for updates at startup</source>
        <translation>Sök efter uppdateringar vid start</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="161"/>
        <source>Update</source>
        <translation>Uppdatera</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="165"/>
        <source>Delete all settings and exit</source>
        <translation>Ta bort alla inställningar och avsluta</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="170"/>
        <source>Uninstall</source>
        <translation>Avinstallera</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="177"/>
        <source>Desktop Shortcut</source>
        <translation>Genväg på skrivordet</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="204"/>
        <source>Applications menu Shortcut</source>
        <translation>Genväg i programmenyn</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="219"/>
        <source>Light Theme</source>
        <translation>Ljust tema</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="221"/>
        <source>Dark Theme</source>
        <translation>Mörkt tema</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="429"/>
        <source>Open</source>
        <translation>Öppna</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="431"/>
        <source>Open language file</source>
        <translation>Öppna språkfil</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="436"/>
        <source>Language files (*.qm)</source>
        <translation>Språkfiler (*.qm)</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="586"/>
        <source>All the program&apos;s settings files will be deleted.&lt;br&gt;Do you want to continue?</source>
        <translation>Alla programmets inställningsfiler kommer att tas bort.&lt;br&gt;Vill du fortsätta?</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="598"/>
        <source>Do you want to remove all the program&apos;s&lt;br&gt;settings files before uninstalling?</source>
        <translation>Vill du ta bort alla programmets&lt;br&gt;inställningsfiler innan du avinstallerar?</translation>
    </message>
    <message>
        <location filename="../save.cpp" line="45"/>
        <source>Save NSIS File list</source>
        <translation>Spara NSIS fillista</translation>
    </message>
    <message>
        <location filename="../save.cpp" line="63"/>
        <source>The file exists.&lt;br&gt;Do you want to overwrite the old file with the new file?</source>
        <translation>Filen finns redan.&lt;br&gt;Vill du skriva över den gamla filen med den nya filen?</translation>
    </message>
    <message>
        <location filename="../save.cpp" line="79"/>
        <location filename="../save.cpp" line="98"/>
        <source>Could not save the file.&lt;br&gt;Check your file permissions.</source>
        <translation>Kunde inte spara filen. Kontrollera dina filrättigheter.</translation>
    </message>
    <message>
        <location filename="../save.cpp" line="115"/>
        <source>ERROR 3: An unexpected error occurred.&lt;br&gt;Could not save the file.</source>
        <translation>FEL 3: Ett oväntat fel inträffade.&lt;br&gt;Det gick inte att spara filen.</translation>
    </message>
    <message>
        <location filename="../deletesettings.cpp" line="45"/>
        <source>Yes</source>
        <translation>Ja</translation>
    </message>
    <message>
        <location filename="../deletesettings.cpp" line="46"/>
        <source>No</source>
        <translation>Nej</translation>
    </message>
    <message>
        <location filename="../deletesettings.cpp" line="56"/>
        <source>Failed to delete the program&apos;s settings files.&lt;br&gt;Check your file permissions.</source>
        <translation>Det gick inte att ta bort programmets inställningsfiler.&lt;br&gt;Kontrollera dina filbehörigheter.</translation>
    </message>
    <message>
        <location filename="../createlist.cpp" line="69"/>
        <location filename="../createlist.cpp" line="117"/>
        <location filename="../createlist.cpp" line="259"/>
        <location filename="../createlist.cpp" line="274"/>
        <location filename="../createlist.cpp" line="401"/>
        <location filename="../createlist.cpp" line="410"/>
        <location filename="../deletesettings.cpp" line="58"/>
        <location filename="../save.cpp" line="47"/>
        <location filename="../save.cpp" line="80"/>
        <location filename="../save.cpp" line="99"/>
        <location filename="../save.cpp" line="116"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../save.cpp" line="64"/>
        <source>Overwrite</source>
        <translation>Skriv över</translation>
    </message>
    <message>
        <source>Select directories in any place. (Files in subdirectories will also be included.)</source>
        <translation type="vanished">Välj kataloger var som helst. (Filer i underkataloger kommer också att inkluderas.)</translation>
    </message>
    <message>
        <location filename="../check_for_updates.cpp" line="23"/>
        <location filename="../check_for_updates.cpp" line="34"/>
        <source>Please click on &quot;Update&quot;</source>
        <translation>Klicka på &quot;Uppdatera&quot;</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="272"/>
        <source>The program is intended for creating file lists for NSIS installer program.</source>
        <translation>Programmet är avsett för att skapa fillistor för NSIS installationsprogram.</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="273"/>
        <source>Many thanks to bovirus for the Italian translation. And for many good ideas that have made the program better.</source>
        <translation>Stort tack till bovirus för den italienska översättningen och för många bra idéer som gjort programmet bättre.</translation>
    </message>
</context>
</TS>
