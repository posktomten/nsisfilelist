// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          nsisFileList
//          Copyright (C) 2022 Ingemar Ceicer
//          https://gitlab.com/posktomten/nsisfilelist
//          programmering1@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation version 3.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

/*

#include "dialog.h"
void Dialog::info(QString &version, QString &revision, QString &qt, QString &bitar, QString &builddatetime)
{
#ifdef __MINGW32__
    version = (QString(tr("Compiler: MinGW (GCC for Windows) version ") + " %1.%2.%3")
               .arg(__GNUC__)
               .arg(__GNUC_MINOR__)
               .arg(__GNUC_PATCHLEVEL__));
#endif
#ifdef _MSC_VER
    version = tr("Compiler: Micrsoft Visual C++ version ") + QString::number(_MSC_VER);
    version += tr("<br>Full version number: ") + QString::number(_MSC_FULL_VER);
#endif
#if __cplusplus==202002L
    revision = tr("Programming language: C++") + "<br>" + tr("Version: C++20");
#elif __cplusplus==201703L
    revision = tr("Programming language: C++") + "<br>" + tr("Version: C++17");
#elif __cplusplus==201402L
    revision = tr("Programming language: C++") + "<br>" + tr("Version: C++14");
#elif __cplusplus==201103L
    revision = tr("Programming language: C++") + "<br>" + tr("Version: C++11");
#else
    revision = tr("Programming language: C++") + "<br>" + tr("Version: Unknown")
#endif
    qt = tr("Application framework: Qt version ") + QT_VERSION_STR;
#ifdef Q_PROCESSOR_X86_32 // 32 bit
    bitar = tr("Processor architecture: 32-bit");
#endif
#ifdef Q_PROCESSOR_X86_64 // 64 bit
    bitar = tr("Processor architecture: 64-bit");
#endif
#define BUILD_DATE_TIME __DATE__ " " __TIME__
    builddatetime = tr("Created: ") + BUILD_DATE_TIME;
    // qDebug() << __cplusplus;
}
*/


