// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          nsisFileList
//          Copyright (C) 2022 - 2024 Ingemar Ceicer
//          https://gitlab.com/posktomten/nsisfilelist
//          programming@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation version 3.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
#include "dialog.h"
#include "qtranslator.h"
#include <QObject>
#include <QApplication>
#include <QSettings>
#include <QFontDatabase>
#include <QStyleFactory>
#include <QStyle>
#include <QStyleHints>

int main(int argc, char *argv[])
{
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QCoreApplication::setAttribute(Qt::AA_UseHighDpiPixmaps);
#endif
    QApplication *a = new QApplication(argc, argv);
    /*    */
#if defined(Q_OS_LINUX)
    a->setStyle(QStyleFactory::create("Fusion"));
#endif
    QSettings settings(QSettings::IniFormat, QSettings::UserScope,
                       DISPLAY_NAME, EXECUTABLE_NAME);
#if QT_VERSION > QT_VERSION_CHECK(6, 0, 0)

    // if(a->palette().window().color().value() < a->palette().windowText().color().value()) {
    if(QApplication::styleHints()->colorScheme() == Qt::ColorScheme::Dark) {
        QPalette palette;
        palette = QApplication::palette();
        palette.setColor(QPalette::Link, Qt::red);
        QApplication::setPalette(palette);
    } else {
        QPalette palette;
        palette = QApplication::palette();
        palette.setColor(QPalette::Link, Qt::blue);
        QApplication::setPalette(palette);
    }

#endif
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    settings.beginGroup("Settings");
    QString theme = settings.value("theme", "system").toString();
    settings.endGroup();

    if(theme == "dark") {
        // Ange det mörka temat för Fusion
        QPalette darkPalette;
        darkPalette.setColor(QPalette::Window, QColor(53, 53, 53));
        darkPalette.setColor(QPalette::WindowText, Qt::white);
        darkPalette.setColor(QPalette::Base, QColor(25, 25, 25));
        darkPalette.setColor(QPalette::AlternateBase, QColor(53, 53, 53));
        darkPalette.setColor(QPalette::ToolTipBase, Qt::black);
        darkPalette.setColor(QPalette::ToolTipText, Qt::white);
        darkPalette.setColor(QPalette::Text, Qt::white);
        darkPalette.setColor(QPalette::Button, QColor(53, 53, 53));
        darkPalette.setColor(QPalette::ButtonText, Qt::white);
        darkPalette.setColor(QPalette::BrightText, Qt::red);
        darkPalette.setColor(QPalette::Link, Qt::red);
        darkPalette.setColor(QPalette::Highlight, QColor(42, 130, 218));
        darkPalette.setColor(QPalette::HighlightedText, Qt::black);
        a->setPalette(darkPalette);
    } else  {
        a->setPalette(a->style()->standardPalette());
    }

#endif
    /*   */
    settings.beginGroup("Settings");
    bool dontusenativedialog = settings.value("dontusenativedialog").toBool();
    settings.endGroup();

    if(dontusenativedialog) {
        QCoreApplication::setAttribute(Qt::AA_DontUseNativeDialogs);
    }

    int id = QFontDatabase::addApplicationFont(QStringLiteral(":/fonts/segoeui.ttf"));
    QString family = QFontDatabase::applicationFontFamilies(id).at(0);
    QFont *font = new QFont(family);
    font->setFamily(family);
    font->setPointSize(FONTSIZE);
    a->setFont(*font);
    QString currentLocale(QLocale::system().name());
    settings.beginGroup("Language");
    QString language = settings.value("language", currentLocale).toString();
    settings.endGroup();
    settings.beginGroup("Settings");
    bool staysontophint = settings.value("staysontophint", true).toBool();
    settings.endGroup();
    const QString qttranslationsPath(QStringLiteral(":/i18n/"));
    QTranslator *qtTranslator = new QTranslator;
//    qDebug() << language;

    if(language == "custom") {
        settings.beginGroup("Language");
        QString customlanguagepath = settings.value("customlanguagepath").toString();
        settings.endGroup();

        if(qtTranslator->load(customlanguagepath)) {
            QApplication::installTranslator(qtTranslator);
        }
    } else  {
        if(qtTranslator->load(qttranslationsPath + "nsisfilelist_" + language + ".qm")) {
            QApplication::installTranslator(qtTranslator);
        }
    }

    const QString qtBasetranslationsPath(QStringLiteral(":/i18n/qttranslations"));
    QTranslator qtBaseTranslator;

    if(qtBaseTranslator.load(qtBasetranslationsPath + "/qtbase_" + language + ".qm")) {
        QApplication::installTranslator(&qtBaseTranslator);
    }

    Dialog *w = new Dialog;

    if(staysontophint) {
        w->setWindowFlags(Qt::MSWindowsFixedSizeDialogHint | Qt::WindowStaysOnTopHint);
    } else {
        w->setWindowFlags(Qt::MSWindowsFixedSizeDialogHint);
    }

#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    w->setWindowFlags(w->windowFlags() & ~Qt::WindowContextHelpButtonHint);
#endif
    w->setFont(*font);
    QObject::connect(a, &QCoreApplication::aboutToQuit, w, &Dialog::setEndConfig);
    w->show();
    int slut = a->exec();
    delete qtTranslator;
    return slut;
}
