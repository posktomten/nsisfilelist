
#// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
#//
#//          nsisFileList
#//          Copyright (C) 2022 - 2024 Ingemar Ceicer
#//          https://gitlab.com/posktomten/nsisfilelist
#//          programming@ceicer.com
#//
#//   This program is free software: you can redistribute it and/or modify
#//   it under the terms of the GNU General Public License as published by
#//   the Free Software Foundation version 3.
#//
#//   This program is distributed in the hope that it will be useful,
#//   but WITHOUT ANY WARRANTY; without even the implied warranty of
#//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#//   GNU General Public License for more details.
#//
#// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

QT      += core gui widgets network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets


# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

TARGET = nsisfilelist




SOURCES += \
    createlist.cpp \
    deletesettings.cpp \
    main.cpp \
    dialog.cpp \
    check_for_updates.cpp \
    save.cpp

HEADERS += \
    dialog.h

FORMS += \
    dialog.ui

TRANSLATIONS += \
    i18n/_nsisfilelist_sv_SE.ts \
    i18n/_nsisfilelist_it_IT.ts \
    i18n/_nsisfilelist_template_xx_XX.ts

#UI_DIR = ../code

#CONFIG += lrelease
#CONFIG += embed_translations

RESOURCES += \
    resources.qrc

RC_FILE = myapp.rc

INCLUDEPATH += "../include"
DEPENDPATH += "../include"

equals(QT_MAJOR_VERSION, 5) {
DESTDIR="../build-executable5"
}

# INCLUDEPATH += "../include_Qt5"
# DEPENDPATH += "../include_Qt5"




contains(QT_ARCH, x86_64) {
win32-g++ {
message (GCC 64-bit)

    # CONFIG: LIBS += -L../lib5/ -lssl
    # CONFIG: LIBS += -L../lib5/ -lcrypto

    CONFIG (release, debug|release): LIBS += -L../lib5/ -lcheckupdate # Release
    # else: CONFIG (debug, debug|release): LIBS += -L../lib5/ -lcheckupdated # Debug

    CONFIG (release, debug|release): LIBS += -L../lib5/ -lcreateshortcut  # Release
    # else: CONFIG (debug, debug|release): LIBS += -L../lib5/ -lcreateshortcutd # Debug

    CONFIG (release, debug|release): LIBS += -L../lib5/ -ldownload_install # Release
    # else: CONFIG (debug, debug|release): LIBS += -L../lib5/ -ldownload_installd # Debug

    CONFIG (release, debug|release): LIBS += -L../lib5/ -labout # Release
    # else: CONFIG (debug, debug|release): LIBS += -L../lib5/ -laboutd # Debug

}
}
contains(QT_ARCH, i386) {
win32-g++ {

message (GCC 32-bit)
    # CONFIG: LIBS += -L../lib5/ -lssl
    # CONFIG: LIBS += -L../lib5/ -lcrypto

    CONFIG (release, debug|release): LIBS += -L../lib5_32/ -lcheckupdate # Release
    else: CONFIG (debug, debug|release): LIBS += -L../lib5_32/ -lcheckupdated # Debug

    CONFIG (release, debug|release): LIBS += -L../lib5_32/ -lcreateshortcut  # Release
    else: CONFIG (debug, debug|release): LIBS += -L../lib5_32/ -lcreateshortcutd # Debug

    CONFIG (release, debug|release): LIBS += -L../lib5_32/ -ldownload_install # Release
    else: CONFIG (debug, debug|release): LIBS += -L../lib5_32/ -ldownload_installd # Debug

    CONFIG (release, debug|release): LIBS += -L../lib5_32/ -labout # Release
    else: CONFIG (debug, debug|release): LIBS += -L../lib5_32/ -laboutd # Debug

}
}
contains(QT_ARCH, x86_64) {
win32-msvc {
message (MSVC 64-bit)
    # CONFIG: LIBS += -L../lib5_msvc/ -lssl
    # CONFIG: LIBS += -L../lib5_msvc/ -lcrypto

    CONFIG (release, debug|release): LIBS += -L../lib5_msvc/ -lcheckupdate # Release
    else: CONFIG (debug, debug|release): LIBS += -L../lib5_msvc/ -lcheckupdated # Debug

    CONFIG (release, debug|release): LIBS += -L../lib5_msvc/ -lcreateshortcut  # Release
    else: CONFIG (debug, debug|release): LIBS += -L../lib5_msvc/ -lcreateshortcutd # Debug

    CONFIG (release, debug|release): LIBS += -L../lib5_msvc/ -ldownload_install # Release
    else: CONFIG (debug, debug|release): LIBS += -L../lib5_msvc/ -ldownload_installd # Debug

    CONFIG (release, debug|release): LIBS += -L../lib5_msvc/ -labout # Release
    else: CONFIG (debug, debug|release): LIBS += -L../lib5_msvc/ -laboutd # Debug
    }
}


contains(QT_ARCH, i386) {
win32-msvc {
message (MSVC 32-bit)
    # CONFIG: LIBS += -L../lib5_msvc/ -lssl
    # CONFIG: LIBS += -L../lib5_msvc/ -lcrypto

    CONFIG (release, debug|release): LIBS += -L../lib5_msvc_32/ -lcheckupdate # Release
    else: CONFIG (debug, debug|release): LIBS += -L../lib5_msvc_32/ -lcheckupdated # Debug

    CONFIG (release, debug|release): LIBS += -L../lib5_msvc_32/ -lcreateshortcut  # Release
    else: CONFIG (debug, debug|release): LIBS += -L../lib5_msvc_32/ -lcreateshortcutd # Debug

    CONFIG (release, debug|release): LIBS += -L../lib5_msvc_32/ -ldownload_install # Release
    else: CONFIG (debug, debug|release): LIBS += -L../lib5_msvc_32/ -ldownload_installd # Debug

    CONFIG (release, debug|release): LIBS += -L../lib5_msvc_32/ -labout # Release
    else: CONFIG (debug, debug|release): LIBS += -L../lib5_msvc_32/ -laboutd # Debug
    }
}




# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

message (----------------------------------------)
message (OS: $$QMAKE_HOST.os)
#message (Arch: $$QMAKE_HOST.arch)
#message (Cpu count: $$QMAKE_HOST.cpu_count)
#message (Name: $$QMAKE_HOST.name)
#message (Version: $$QMAKE_HOST.version)
#message (Version string: $$QMAKE_HOST.version_string)
compiler=$$basename(QMAKESPEC)
message(compiler: $$compiler)
message (QMAKE_QMAKE: $$QMAKE_QMAKE)
#message (QT_VERSION: $$QT_VERSION)
message(_PRO_FILE_PWD_: $$_PRO_FILE_PWD_)
message(TEMPLATE: $$TEMPLATE)
message(TARGET: $$TARGET)
message(DESTDIR: $$DESTDIR)
#message(HEADERS $$HEADERS)
#message(SOURCES: $$SOURCES)
#message(INCLUDEPATH $$INCLUDEPATH)
message(LIBS: $$LIBS)
