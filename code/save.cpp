// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          nsisFileList
//          Copyright (C) 2022 - 2024 Ingemar Ceicer
//          https://gitlab.com/posktomten/nsisfilelist
//          programming@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation version 3.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#include <QFileDialog>
#include <QStandardPaths>
#include <QSettings>
#include <QTemporaryFile>
#include <QMessageBox>
#include "dialog.h"

void Dialog::save()
{
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME, EXECUTABLE_NAME);
    settings.beginGroup("Paths");
    QString savepath = settings.value("savepath", QStandardPaths::writableLocation(QStandardPaths::HomeLocation)).toString();
    settings.endGroup();
    settings.beginGroup("Geometry");
    QRect filedialoggeometry = settings.value("filedialoggeometry", QRect(x(), y(), 900, 600)).toRect();
    settings.endGroup();
//    const QStringList filters({tr("Text files") + "(*.txt)",
//                               tr("nsh files") + "(*.nsh)",
//                               tr("Any files") + "(*)"
//                              });
//    const QString defaultsufix = QStringLiteral("*");
    QFileDialog *fileDialog = new QFileDialog(this);
    fileDialog->setGeometry(filedialoggeometry);
//    fileDialog->setDefaultSuffix(defaultsufix);
//    fileDialog->setNameFilters(filters);
    fileDialog->setDirectory(savepath);
    fileDialog->setWindowTitle(tr("Save NSIS File list"));
//        fileDialog->setOption(QFileDialog::DontUseNativeDialog, true);
    fileDialog->setLabelText(QFileDialog::Accept, tr("Ok"));
    fileDialog->setLabelText(QFileDialog::Reject, tr("Cancel"));
    fileDialog->setViewMode(QFileDialog::Detail);
    fileDialog->setFileMode(QFileDialog::AnyFile);

    if(fileDialog->exec() == 1) {
        QString newfile = fileDialog->selectedFiles().at(0);
        QFileInfo fi(newfile);

        if(fi.exists()) {
            QMessageBox msgBox;
            msgBox.setWindowFlags(Qt::WindowStaysOnTopHint | Qt::WindowSystemMenuHint);
            msgBox.setIcon(QMessageBox::Critical);
            msgBox.setWindowTitle(DISPLAY_NAME " " DISPLAY_VERSION);
            msgBox.setText(tr("The file exists.<br>Do you want to overwrite the old file with the new file?"));
            msgBox.addButton(tr("Overwrite"), QMessageBox::AcceptRole);
            msgBox.addButton(tr("Cancel"), QMessageBox::RejectRole);

            if(msgBox.exec() == QMessageBox::RejectRole) {
                settings.beginGroup("Geometry");
                settings.setValue("filedialoggeometry", fileDialog->geometry());
                settings.endGroup();
                return;
            } else {
                if(!QFile::remove(newfile)) {
                    QMessageBox msgBox;
                    msgBox.setWindowFlags(Qt::WindowStaysOnTopHint | Qt::WindowSystemMenuHint);
                    msgBox.setWindowFlags(Qt::WindowStaysOnTopHint);
                    msgBox.setIcon(QMessageBox::Critical);
                    msgBox.setWindowTitle(DISPLAY_NAME " " DISPLAY_VERSION);
                    msgBox.setText(tr("Could not save the file.<br>Check your file permissions."));
                    msgBox.addButton(tr("Ok"), QMessageBox::AcceptRole);
                    msgBox.exec();
                    settings.beginGroup("Geometry");
                    settings.setValue("filedialoggeometry", fileDialog->geometry());
                    settings.endGroup();
                    return;
                }
            }
        }

        QString path = fi.absolutePath();
        QFileInfo fi1(path);

        if(!fi1.isWritable()) {
            QMessageBox msgBox;
            msgBox.setWindowFlags(Qt::WindowStaysOnTopHint | Qt::WindowSystemMenuHint);
            msgBox.setIcon(QMessageBox::Critical);
            msgBox.setWindowTitle(DISPLAY_NAME " " DISPLAY_VERSION);
            msgBox.setText(tr("Could not save the file.<br>Check your file permissions."));
            msgBox.addButton(tr("Ok"), QMessageBox::AcceptRole);
            msgBox.exec();
            settings.beginGroup("Geometry");
            settings.setValue("filedialoggeometry", fileDialog->geometry());
            settings.endGroup();
            return;
        } else {
            settings.beginGroup("Paths");
            settings.setValue("savepath", path);
            settings.endGroup();

            if(!tmpFile->copy(fileDialog->selectedFiles().at(0))) {
                QMessageBox msgBox;
                msgBox.setWindowFlags(Qt::WindowStaysOnTopHint | Qt::WindowSystemMenuHint);
                msgBox.setIcon(QMessageBox::Critical);
                msgBox.setWindowTitle(DISPLAY_NAME " " DISPLAY_VERSION);
                msgBox.setText(tr("ERROR 3: An unexpected error occurred.<br>Could not save the file."));
                msgBox.addButton(tr("Ok"), QMessageBox::AcceptRole);
                msgBox.exec();
                settings.beginGroup("Geometry");
                settings.setValue("filedialoggeometry", fileDialog->geometry());
                settings.endGroup();
                tmpFile->deleteLater();
                ui->pbSave->setDisabled(true);
                return;
            } else {
                settings.beginGroup("Geometry");
                settings.setValue("filedialoggeometry", fileDialog->geometry());
                settings.endGroup();
                tmpFile->deleteLater();
                ui->pbSave->setDisabled(true);
                return;
            }
        }
    }
}
