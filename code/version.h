
#ifndef VERSION_H
#define VERSION_H

#define VER_FILEVERSION             1,3,1,0
#define VER_FILEVERSION_STR         "1.3.1.0\0"

#define VER_PRODUCTVERSION          1,3,1,0
#define VER_PRODUCTVERSION_STR      "1.3.1\0"

#define VER_COMPANYNAME_STR         "Ingemar Ceicer"
#define VER_FILEDESCRIPTION_STR     "Creates file list for NSIS."
#define VER_INTERNALNAME_STR        "streamCapture2"
#define VER_LEGALCOPYRIGHT_STR      "Copyright (C) 2022 - 2024 Ingemar Ceicer"
#define VER_LEGALTRADEMARKS1_STR    "All Rights Reserved"
#define VER_LEGALTRADEMARKS2_STR    VER_LEGALTRADEMARKS1_STR
#define VER_ORIGINALFILENAME_STR    "nsisfilelist.exe"
#define VER_PRODUCTNAME_STR         "nsisfilelist"

#define VER_COMPANYDOMAIN_STR       "https://gitlab.com/posktomten/nsisfilelist"

#endif // VERSION_H

