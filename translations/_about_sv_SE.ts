<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="sv_SE">
<context>
    <name>About</name>
    <message>
        <location filename="../about.cpp" line="56"/>
        <source>About </source>
        <translation>Om </translation>
    </message>
    <message>
        <location filename="../about.cpp" line="57"/>
        <source>Copyright © </source>
        <translation>Copyright © </translation>
    </message>
    <message>
        <location filename="../about.cpp" line="57"/>
        <source>License: </source>
        <translation>Licens: </translation>
    </message>
    <message>
        <location filename="../about.cpp" line="58"/>
        <source>is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.</source>
        <translation>distribueras i hopp om att den ska vara användbar, men UTAN NÅGON GARANTI; utan ens underförstådd garanti för SÄLJBARHET eller LÄMPLIGHET FÖR ETT SÄRSKILT SYFTE.</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="59"/>
        <source>Source code</source>
        <translation>Källkod</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="60"/>
        <source>Website</source>
        <translation>Webbsida</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="60"/>
        <source>Version history</source>
        <translation>Versionshistorik</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="63"/>
        <source>Created: </source>
        <translation>Skapad: </translation>
    </message>
    <message>
        <location filename="../about.cpp" line="63"/>
        <source>Compiled on: </source>
        <translation>Kompilerad på: </translation>
    </message>
    <message>
        <location filename="../about.cpp" line="58"/>
        <source>See the GNU General Public License version 3.0 for more details.</source>
        <translation>Se GNU General Public License version 3.0 för mer information.</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="63"/>
        <source>Runs on: </source>
        <translation>Körs på: </translation>
    </message>
    <message>
        <location filename="../about.cpp" line="63"/>
        <source> is in the folder: </source>
        <translation> finns i mappen: </translation>
    </message>
    <message>
        <location filename="../about.cpp" line="70"/>
        <location filename="../about.cpp" line="99"/>
        <location filename="../about.cpp" line="101"/>
        <location filename="../about.cpp" line="103"/>
        <source>Compiler:</source>
        <translation>Kompilator:</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="85"/>
        <source>Compiler: LLVM/Clang/LLD based MinGW version </source>
        <translation>Kompilator: LLVM/Clang/LLD baserad MinGW version </translation>
    </message>
    <message>
        <location filename="../about.cpp" line="91"/>
        <source>Compiler: MinGW (GCC for Windows) version </source>
        <translation>Kompilator: MinGW (GCC för Windows) version </translation>
    </message>
    <message>
        <location filename="../about.cpp" line="108"/>
        <location filename="../about.cpp" line="110"/>
        <location filename="../about.cpp" line="112"/>
        <location filename="../about.cpp" line="114"/>
        <location filename="../about.cpp" line="116"/>
        <source>Programming language: C++</source>
        <translation>Programmeringsspråk: C++</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="108"/>
        <source>C++ version: C++20</source>
        <translation>C++ version: C++20</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="110"/>
        <source>C++ version: C++17</source>
        <translation>C++ version: C++17</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="112"/>
        <source>C++ version: C++14</source>
        <translation>C++ version: C++14</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="114"/>
        <source>C++ version: C++11</source>
        <translation>C++ version: C++11</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="116"/>
        <source>C++ version: Unknown</source>
        <translation>C++ version: Okänd</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="118"/>
        <source>Application framework: Qt version </source>
        <translation>Applikationsramverk: Qt version </translation>
    </message>
    <message>
        <location filename="../about.cpp" line="120"/>
        <source>Processor architecture: 32-bit</source>
        <translation>Processorarkitektur: 32-bitar</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="123"/>
        <source>Processor architecture: 64-bit</source>
        <translation>Processorarkitektur: 64-bitar</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="78"/>
        <source>Compiler: Clang version </source>
        <translation>Kompilator: Clang version </translation>
    </message>
</context>
</TS>
