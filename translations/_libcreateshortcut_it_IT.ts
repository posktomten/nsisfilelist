<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="it_IT">
<context>
    <name>Createshortcut</name>
    <message>
        <location filename="../createshortcut.cpp" line="36"/>
        <location filename="../createshortcut.cpp" line="52"/>
        <source>Shortcut could not be created in</source>
        <translation>Impossibile creare il collegamento in</translation>
    </message>
    <message>
        <location filename="../createshortcut.cpp" line="37"/>
        <location filename="../createshortcut.cpp" line="53"/>
        <location filename="../removeshortcut.cpp" line="34"/>
        <location filename="../removeshortcut.cpp" line="48"/>
        <source>Ok</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../removeshortcut.cpp" line="33"/>
        <location filename="../removeshortcut.cpp" line="47"/>
        <source>The shortcut could not be removed:</source>
        <translation>Impossibile rimuovere il collegamento:</translation>
    </message>
</context>
</TS>
