<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="sv_SE">
<context>
    <name>Createshortcut</name>
    <message>
        <location filename="../createshortcut.cpp" line="36"/>
        <location filename="../createshortcut.cpp" line="52"/>
        <source>Shortcut could not be created in</source>
        <translation>Det gick inte att skapa genväg i</translation>
    </message>
    <message>
        <location filename="../createshortcut.cpp" line="37"/>
        <location filename="../createshortcut.cpp" line="53"/>
        <location filename="../deleteallsettings.cpp" line="86"/>
        <location filename="../deleteallsettings.cpp" line="99"/>
        <location filename="../deleteallsettings.cpp" line="113"/>
        <location filename="../deleteallsettings.cpp" line="141"/>
        <location filename="../removeshortcut.cpp" line="34"/>
        <location filename="../removeshortcut.cpp" line="49"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../deleteallsettings.cpp" line="29"/>
        <source>&lt;b&gt;Warning!&lt;/b&gt;</source>
        <translation>&lt;b&gt;Varning!&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../deleteallsettings.cpp" line="32"/>
        <source>No</source>
        <translation>Nej</translation>
    </message>
    <message>
        <location filename="../deleteallsettings.cpp" line="33"/>
        <source>Yes</source>
        <translation>Ja</translation>
    </message>
    <message>
        <location filename="../deleteallsettings.cpp" line="44"/>
        <source>All your saved settings will be deleted.&lt;br&gt;The desktop shortcut will be removed.&lt;br&gt;And the program will exit.&lt;br&gt;Do you want to continue?</source>
        <translation>Alla dina sparade inställningar kommer att raderas.&lt;br&gt;Genvägen på skrivbordet kommer att tas bort.&lt;br&gt;Och programmet avslutas.&lt;br&gt;Vill du fortsätta?</translation>
    </message>
    <message>
        <location filename="../deleteallsettings.cpp" line="47"/>
        <source>All your saved settings will be deleted.&lt;br&gt;And the program will exit.&lt;br&gt;Do you want to continue?</source>
        <translation>Alla dina sparade inställningar kommer att raderas.&lt;br&gt;Och programmet avslutas.&lt;br&gt;Vill du fortsätta?</translation>
    </message>
    <message>
        <location filename="../deleteallsettings.cpp" line="50"/>
        <source>All your saved settings will be deleted.&lt;br&gt;All shortcuts will be removed.&lt;br&gt;And the program will exit.&lt;br&gt;Do you want to continue?</source>
        <translation>Alla dina sparade inställningar kommer att raderas.&lt;br&gt;Alla genvägar kommer att tas bort.&lt;br&gt;Och programmet avslutas.&lt;br&gt;Vill du fortsätta?</translation>
    </message>
    <message>
        <location filename="../deleteallsettings.cpp" line="84"/>
        <location filename="../deleteallsettings.cpp" line="139"/>
        <source>Failed to delete your configuration files.&lt;br&gt;Check your file permissions.</source>
        <translation>Det gick inte att ta bort dina konfigurationsfiler.&lt;br&gt;Kontrollera dina filbehörigheter.</translation>
    </message>
    <message>
        <location filename="../deleteallsettings.cpp" line="98"/>
        <location filename="../deleteallsettings.cpp" line="112"/>
        <source> can not be removed.&lt;br&gt;Pleas check your file permissions.</source>
        <translation> kan inte tas bort.&lt;br&gt;Kontrollera dina filbehörigheter.</translation>
    </message>
    <message>
        <location filename="../removeshortcut.cpp" line="33"/>
        <location filename="../removeshortcut.cpp" line="48"/>
        <source>The shortcut could not be removed:</source>
        <translation>Genvägen kunde inte tas bort:</translation>
    </message>
</context>
</TS>
