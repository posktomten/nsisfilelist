@echo off

set "LUPDATE_OPTIONS=-noobsolete -locations none -disable-heuristic sametext -disable-heuristic similartext"

set "SOKVAG=C:\Qt\6.8.2\mingw_64\bin"
set "PATH=C:\Qt\Tools\mingw1310_64\bin;PATH"

SET MYPATH=%~dp0
echo %MYPATH%

::%SOKVAG%\lupdate -no-obsolete C:\Users\posktomten\PROGRAMMERING\nsisfilelist\code\nsisfilelist.pro
::%SOKVAG%\lupdate -no-obsolete C:\Users\posktomten\PROGRAMMERING\libabout\code\about.pro
::%SOKVAG%\lupdate -no-obsolete C:\Users\posktomten\PROGRAMMERING\libcheckforupdates\bibliotek\code\checkupdate.pro
::%SOKVAG%\lupdate -no-obsolete C:\Users\posktomten\PROGRAMMERING\download_install\code\download_install.pro
::%SOKVAG%\lupdate -no-obsolete C:\Users\posktomten\PROGRAMMERING\libcreateshortcut\bibliotek\code\createshortcut.pro

copy /y C:\Users\posktomten\PROGRAMMERING\nsisfilelist\code\i18n\*.ts
copy /y C:\Users\posktomten\PROGRAMMERING\libabout\code\i18n\*.ts
copy /y C:\Users\posktomten\PROGRAMMERING\libcheckforupdates\bibliotek\code\i18n\*.ts
copy /y C:\Users\posktomten\PROGRAMMERING\download_install\code\i18n\*.ts
copy /y C:\Users\posktomten\PROGRAMMERING\libcreateshortcut\bibliotek\code\i18n\*.ts




%SOKVAG%\lconvert _libcheckupdate_sv_SE.ts _libcreateshortcut_sv_SE.ts _download_install_sv_SE.ts _about_sv_SE.ts _nsisfilelist_sv_SE.ts  -o nsisfilelist_sv_SE.ts
%SOKVAG%\lconvert _libcheckupdate_it_IT.ts _libcreateshortcut_it_IT.ts _download_install_it_IT.ts _about_it_IT.ts _nsisfilelist_it_IT.ts -o nsisfilelist_it_IT.ts
%SOKVAG%\lconvert _libcheckupdate_template_xx_XX.ts _libcreateshortcut_template_xx_XX.ts  _download_install_template_xx_XX.ts _nsisfilelist_template_xx_XX.ts _about_template_xx_XX.ts -o nsisfilelist_template_xx_XX.ts

%SOKVAG%\lrelease nsisfilelist_sv_SE.ts
copy /y nsisfilelist_sv_SE.qm ..\code\i18n\

%SOKVAG%\lrelease nsisfilelist_it_IT.ts
copy /y nsisfilelist_it_IT.qm ..\code\i18n\
