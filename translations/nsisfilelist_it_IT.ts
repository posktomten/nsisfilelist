<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="it_IT" sourcelanguage="it_IT">
<context>
    <name>CheckUpdate</name>
    <message>
        <location filename="../checkupdate.cpp" line="74"/>
        <location filename="../checkupdate.cpp" line="96"/>
        <location filename="../checkupdate.cpp" line="176"/>
        <location filename="../checkupdate.cpp" line="296"/>
        <source>No Internet connection was found.
Please check your Internet settings and firewall.</source>
        <translation>Nessuna connessione internet disponibile.
Verifica le impostazioni internet ed il firewall.</translation>
    </message>
    <message>
        <location filename="../checkupdate.cpp" line="39"/>
        <location filename="../checkupdate.cpp" line="53"/>
        <location filename="../checkupdate.cpp" line="71"/>
        <location filename="../checkupdate.cpp" line="97"/>
        <location filename="../checkupdate.cpp" line="116"/>
        <location filename="../checkupdate.cpp" line="126"/>
        <location filename="../checkupdate.cpp" line="137"/>
        <location filename="../checkupdate.cpp" line="173"/>
        <location filename="../checkupdate.cpp" line="210"/>
        <location filename="../checkupdate.cpp" line="286"/>
        <location filename="../checkupdate.cpp" line="297"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../checkupdate.cpp" line="114"/>
        <source>Your version of </source>
        <translation>La versione di </translation>
    </message>
    <message>
        <location filename="../checkupdate.cpp" line="115"/>
        <source> is newer than the latest official version. </source>
        <translation> è più recente della versione ufficiale più recente. </translation>
    </message>
    <message>
        <location filename="../checkupdate.cpp" line="125"/>
        <source>You have the latest version of </source>
        <translation>Hai la versione aggiornata di </translation>
    </message>
    <message>
        <location filename="../checkupdate.cpp" line="136"/>
        <source>There was an error when the version was checked.</source>
        <translation>Si è verificato un errore durante il controllo versione.</translation>
    </message>
    <message>
        <location filename="../checkupdate.cpp" line="209"/>
        <source>
There was an error when the version was checked.</source>
        <translation>
Si è verificato un errore durante il controllo versione.</translation>
    </message>
    <message>
        <location filename="../checkupdate.cpp" line="256"/>
        <source>Updates:</source>
        <translation>Aggiornamenti:</translation>
    </message>
    <message>
        <location filename="../checkupdate.cpp" line="282"/>
        <source>There is a new version of </source>
        <translation>È dispoibile una nuova versione di </translation>
    </message>
    <message>
        <location filename="../checkupdate.cpp" line="284"/>
        <source>Latest version: </source>
        <translation>Versione aggiornata: </translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="29"/>
        <source>No error.</source>
        <translation>Nessun errore.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="33"/>
        <source>The remote server closed the connection prematurely, before the entire reply was received and processed.</source>
        <translation>Il server remoto ha chiuso prematuramente la connessione, prima che l&apos;intera risposta fosse ricevuta ed elaborata.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="37"/>
        <source>The remote host name was not found (invalid hostname).</source>
        <translation>Non è stato trovato il nome host remoto (nome host non valido).</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="41"/>
        <source>The connection to the remote server timed out.</source>
        <translation>Timeout della connessione verso il server remoto.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="45"/>
        <source>The operation was canceled via calls to abort() or close() before it was finished.</source>
        <translation>L&apos;operazione è stata annullata tramite chiamate a abort() o close() prima che fosse terminata.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="49"/>
        <source>The SSL/TLS handshake failed and the encrypted channel could not be established. The sslErrors() signal should have been emitted.</source>
        <translation>L&apos;handshake SSL/TLS non è riuscito e non è stato possibile stabilire il canale crittografato.&lt;br&gt;Dovrebbe essere stato emesso il segnale sslErrors().</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="53"/>
        <source>The connection was broken due to disconnection from the network, however the system has initiated roaming to another access point. The request should be resubmitted and will be processed as soon as the connection is re-established.</source>
        <translation>La connessione è stata interrotta a causa della disconnessione dalla rete, tuttavia il sistema ha avviato il roaming verso un altro punto di accesso.&lt;br&gt;La richiesta deve essere ripresentata e sarà elaborata non appena la connessione sarà ristabilita.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="57"/>
        <source>The connection was broken due to disconnection from the network or failure to start the network.</source>
        <translation>La connessione è stata interrotta a causa della disconnessione dalla rete o del mancato avvio della rete.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="61"/>
        <source>The background request is not currently allowed due to platform policy.</source>
        <translation>La richiesta in background non è attualmente consentita a causa della politica della piattaforma.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="65"/>
        <source>While following redirects, the maximum limit was reached. The limit is by default set to 50 or as set by QNetworkRequest::setMaxRedirectsAllowed().</source>
        <translation>È stato raggiunto il limite massimo durante i reindirizzamenti.&lt;br&gt;Per impostazione predefinita, il limite è impostato su 50 o impostato da QNetworkRequest::setMaxRedirectsAllowed().</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="69"/>
        <source>While following redirects, the network access API detected a redirect from a encrypted protocol (https) to an unencrypted one (http).</source>
        <translation>Durante i reindirizzamenti, l&apos;API di accesso alla rete ha rilevato un reindirizzamento da un protocollo crittografato (https) a uno non crittografato (http).</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="73"/>
        <source>An unknown network-related error was detected.</source>
        <translation>È stato rilevato un errore di rete sconosciuto.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="77"/>
        <source>The connection to the proxy server was refused (the proxy server is not accepting requests).</source>
        <translation>La connessione al server proxy è stata rifiutata (il server proxy non accetta richieste).</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="81"/>
        <source>The proxy server closed the connection prematurely, before the entire reply was received and processed.</source>
        <translation>Il server proxy ha chiuso prematuramente la connessione, prima che l&apos;intera risposta fosse ricevuta ed elaborata.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="85"/>
        <source>The proxy host name was not found (invalid proxy hostname).</source>
        <translation>Non è stato trovato il nome host proxy (nome host proxy non valido).</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="89"/>
        <source>The connection to the proxy timed out or the proxy did not reply in time to the request sent.</source>
        <translation>La connessione al proxy è scaduta oppure il proxy non ha risposto in tempo alla richiesta inviata.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="93"/>
        <source>The proxy requires authentication in order to honour the request but did not accept any credentials offered (if any).</source>
        <translation>Per elaborare la richiesta il proxy richiede l&apos;autenticazione ma non ha accettato (se presenti) le credenziali offerte.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="97"/>
        <source>An unknown proxy-related error was detected.</source>
        <translation>È stato rilevato un errore sconosciuto relativo al proxy.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="101"/>
        <source>The access to the remote content was denied (similar to HTTP error 403).</source>
        <translation>L&apos;accesso al contenuto remoto è stato negato (simile all&apos;errore HTTP 403).</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="105"/>
        <source>The operation requested on the remote content is not permitted.</source>
        <translation>L&apos;operazione richiesta sul contenuto remoto non è consentita.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="109"/>
        <source>The remote content was not found at the server (similar to HTTP error 404).</source>
        <translation>Nel server non è stato trovato il contenuto remoto (simile all&apos;errore HTTP 404).</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="113"/>
        <source>The remote server requires authentication to serve the content but the credentials provided were not accepted (if any).</source>
        <translation>Per elaborare la richiesta il server remoto richiede l&apos;autenticazione ma le credenziali fornite (se presenti) non sono state accettate.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="117"/>
        <source>The request needed to be sent again, but this failed for example because the upload data could not be read a second time.</source>
        <translation>La richiesta doveva essere inviata di nuovo, ma questo non è riuscito ad esempio perché non è stato possibile leggere i dati di caricamento una seconda volta.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="121"/>
        <source>The request could not be completed due to a conflict with the current state of the resource.</source>
        <translation>Impossibile completare la richiesta a causa di un conflitto con lo stato attuale della risorsa.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="125"/>
        <source>The requested resource is no longer available at the server.</source>
        <translation>La risorsa richiesta non è più disponibile nel server.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="129"/>
        <source>An unknown error related to the remote content was detected.</source>
        <translation>È stato rilevato un errore sconosciuto relativo al contenuto remoto.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="133"/>
        <source>The Network Access API cannot honor the request because the protocol is not known.</source>
        <translation>L&apos;API di accesso alla rete non può soddisfare la richiesta perché il protocollo è sconosciuto.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="137"/>
        <source>The requested operation is invalid for this protocol.</source>
        <translation>L&apos;operazione richiesta non è valida per questo protocollo.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="141"/>
        <source>A breakdown in protocol was detected (parsing error, invalid or unexpected responses, etc.).</source>
        <translation>È stata rilevata un&apos;interruzione del protocollo (errore di analisi, risposte non valide o impreviste, ecc.).</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="145"/>
        <source>The server encountered an unexpected condition which prevented it from fulfilling the request.</source>
        <translation>Il server ha riscontrato una condizione imprevista che gli ha impedito di soddisfare la richiesta.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="149"/>
        <source>The server does not support the functionality required to fulfill the request.</source>
        <translation>Il server non supporta la funzionalità necessaria per soddisfare la richiesta.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="153"/>
        <source>The server is unable to handle the request at this time.</source>
        <translation>In questo momento il server non è in grado di gestire la richiesta.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="157"/>
        <source>Unknown Server Error.</source>
        <translation>Errore server sconosciuto.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="161"/>
        <source>The remote server refused the connection (the server is not accepting requests).</source>
        <translation>Il server remoto ha rifiutato la connessione (il server non accetta richieste).</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="165"/>
        <source>Unknown Error.</source>
        <translation>Errore sconosciuto.</translation>
    </message>
</context>
<context>
    <name>Createshortcut</name>
    <message>
        <location filename="../createshortcut.cpp" line="36"/>
        <location filename="../createshortcut.cpp" line="52"/>
        <source>Shortcut could not be created in</source>
        <translation>Impossibile creare il collegamento in</translation>
    </message>
    <message>
        <location filename="../createshortcut.cpp" line="37"/>
        <location filename="../createshortcut.cpp" line="53"/>
        <location filename="../removeshortcut.cpp" line="34"/>
        <location filename="../removeshortcut.cpp" line="48"/>
        <source>Ok</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../removeshortcut.cpp" line="33"/>
        <location filename="../removeshortcut.cpp" line="47"/>
        <source>The shortcut could not be removed:</source>
        <translation>Impossibile rimuovere il collegamento:</translation>
    </message>
</context>
<context>
    <name>DownloadInstall</name>
    <message>
        <location filename="../download_install.ui" line="20"/>
        <source>Dialog</source>
        <translation>Finestra</translation>
    </message>
    <message>
        <location filename="../download_install.ui" line="54"/>
        <source>Downloading...</source>
        <translation>Download...</translation>
    </message>
    <message>
        <location filename="../download_install.ui" line="70"/>
        <source>Cancel</source>
        <translation>Annulla</translation>
    </message>
    <message>
        <location filename="../download_install.ui" line="86"/>
        <source>Install</source>
        <translation>Installa</translation>
    </message>
    <message>
        <location filename="../download_install.cpp" line="42"/>
        <source>Download </source>
        <translation>Download </translation>
    </message>
    <message>
        <location filename="../download_install.cpp" line="57"/>
        <location filename="../download_install.cpp" line="70"/>
        <source>Ok</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../download_install.cpp" line="90"/>
        <source>The download is complete.&lt;br&gt;The current version will be uninstalled before the new one is installed.</source>
        <translation>Il download è stata completato.&lt;br&gt;Prima verrà disinstallata la versione attuale quindi installata la nuova versione.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="28"/>
        <source>No error.</source>
        <translation>Nessun errore.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="32"/>
        <source>The remote server closed the connection prematurely, before the entire reply was received and processed.</source>
        <translation>Il server remoto ha chiuso prematuramente la connessione, prima che l&apos;intera risposta fosse ricevuta ed elaborata.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="36"/>
        <source>The remote host name was not found (invalid hostname).</source>
        <translation>Non è stato trovato il nome host remoto (nome host non valido).</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="40"/>
        <source>The connection to the remote server timed out.</source>
        <translation>Timeout della connessione verso il server remoto.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="44"/>
        <source>The operation was canceled via calls to abort() or close() before it was finished.</source>
        <translation>L&apos;operazione è stata annullata tramite chiamate a abort() o close() prima che fosse terminata.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="48"/>
        <source>The SSL/TLS handshake failed and the encrypted channel could not be established. The sslErrors() signal should have been emitted.</source>
        <translation>L&apos;handshake SSL/TLS non è riuscito e non è stato possibile stabilire il canale crittografato.&lt;br&gt;Dovrebbe essere stato emesso il segnale sslErrors().</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="52"/>
        <source>The connection was broken due to disconnection from the network, however the system has initiated roaming to another access point. The request should be resubmitted and will be processed as soon as the connection is re-established.</source>
        <translation>La connessione è stata interrotta a causa della disconnessione dalla rete, tuttavia il sistema ha avviato il roaming verso un altro punto di accesso.&lt;br&gt;La richiesta deve essere ripresentata e sarà elaborata non appena la connessione sarà ristabilita.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="56"/>
        <source>The connection was broken due to disconnection from the network or failure to start the network.</source>
        <translation>La connessione è stata interrotta a causa della disconnessione dalla rete o del mancato avvio della rete.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="60"/>
        <source>The background request is not currently allowed due to platform policy.</source>
        <translation>La richiesta in background non è attualmente consentita a causa della politica della piattaforma.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="64"/>
        <source>While following redirects, the maximum limit was reached. The limit is by default set to 50 or as set by QNetworkRequest::setMaxRedirectsAllowed().</source>
        <translation>È stato raggiunto il limite massimo durante i reindirizzamenti.&lt;br&gt;Per impostazione predefinita, il limite è impostato su 50 o impostato da QNetworkRequest::setMaxRedirectsAllowed().</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="68"/>
        <source>While following redirects, the network access API detected a redirect from a encrypted protocol (https) to an unencrypted one (http).</source>
        <translation>Durante i reindirizzamenti, l&apos;API di accesso alla rete ha rilevato un reindirizzamento da un protocollo crittografato (https) a uno non crittografato (http).</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="72"/>
        <source>An unknown network-related error was detected.</source>
        <translation>È stato rilevato un errore di rete sconosciuto.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="76"/>
        <source>The connection to the proxy server was refused (the proxy server is not accepting requests).</source>
        <translation>La connessione al server proxy è stata rifiutata (il server proxy non accetta richieste).</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="80"/>
        <source>The proxy server closed the connection prematurely, before the entire reply was received and processed.</source>
        <translation>Il server proxy ha chiuso prematuramente la connessione, prima che l&apos;intera risposta fosse ricevuta ed elaborata.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="84"/>
        <source>The proxy host name was not found (invalid proxy hostname).</source>
        <translation>Non è stato trovato il nome host proxy (nome host proxy non valido).</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="88"/>
        <source>The connection to the proxy timed out or the proxy did not reply in time to the request sent.</source>
        <translation>La connessione al proxy è scaduta oppure il proxy non ha risposto in tempo alla richiesta inviata.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="92"/>
        <source>The proxy requires authentication in order to honour the request but did not accept any credentials offered (if any).</source>
        <translation>Per elaborare la richiesta il proxy richiede l&apos;autenticazione ma non ha accettato (se presenti) le credenziali offerte.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="96"/>
        <source>An unknown proxy-related error was detected.</source>
        <translation>È stato rilevato un errore sconosciuto relativo al proxy.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="100"/>
        <source>The access to the remote content was denied (similar to HTTP error 403).</source>
        <translation>L&apos;accesso al contenuto remoto è stato negato (simile all&apos;errore HTTP 403).</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="104"/>
        <source>The operation requested on the remote content is not permitted.</source>
        <translation>L&apos;operazione richiesta sul contenuto remoto non è consentita.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="108"/>
        <source>The remote content was not found at the server (similar to HTTP error 404).</source>
        <translation>Nel server non è stato trovato il contenuto remoto (simile all&apos;errore HTTP 404).</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="112"/>
        <source>The remote server requires authentication to serve the content but the credentials provided were not accepted (if any).</source>
        <translation>Per elaborare la richiesta il server remoto richiede l&apos;autenticazione ma le credenziali fornite (se presenti) non sono state accettate.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="116"/>
        <source>The request needed to be sent again, but this failed for example because the upload data could not be read a second time.</source>
        <translation>La richiesta doveva essere inviata di nuovo, ma questo non è riuscito ad esempio perché non è stato possibile leggere i dati di caricamento una seconda volta.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="120"/>
        <source>The request could not be completed due to a conflict with the current state of the resource.</source>
        <translation>Impossibile completare la richiesta a causa di un conflitto con lo stato attuale della risorsa.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="124"/>
        <source>The requested resource is no longer available at the server.</source>
        <translation>La risorsa richiesta non è più disponibile nel server.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="128"/>
        <source>An unknown error related to the remote content was detected.</source>
        <translation>È stato rilevato un errore sconosciuto relativo al contenuto remoto.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="132"/>
        <source>The Network Access API cannot honor the request because the protocol is not known.</source>
        <translation>L&apos;API di accesso alla rete non può soddisfare la richiesta perché il protocollo è sconosciuto.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="136"/>
        <source>The requested operation is invalid for this protocol.</source>
        <translation>L&apos;operazione richiesta non è valida per questo protocollo.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="140"/>
        <source>A breakdown in protocol was detected (parsing error, invalid or unexpected responses, etc.).</source>
        <translation>È stata rilevata un&apos;interruzione del protocollo (errore di analisi, risposte non valide o impreviste, ecc.).</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="144"/>
        <source>The server encountered an unexpected condition which prevented it from fulfilling the request.</source>
        <translation>Il server ha riscontrato una condizione imprevista che gli ha impedito di soddisfare la richiesta.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="148"/>
        <source>The server does not support the functionality required to fulfill the request.</source>
        <translation>Il server non supporta la funzionalità necessaria per soddisfare la richiesta.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="152"/>
        <source>The server is unable to handle the request at this time.</source>
        <translation>In questo momento il server non è in grado di gestire la richiesta.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="156"/>
        <source>Unknown Server Error.</source>
        <translation>Errore server sconosciuto.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="160"/>
        <source>The remote server refused the connection (the server is not accepting requests).</source>
        <translation>Il server remoto ha rifiutato la connessione (il server non accetta richieste).</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="164"/>
        <source>Unknown Error.</source>
        <translation>Errore sconosciuto.</translation>
    </message>
</context>
<context>
    <name>About</name>
    <message>
        <location filename="../about.cpp" line="56"/>
        <source>About </source>
        <translation>Info su </translation>
    </message>
    <message>
        <location filename="../about.cpp" line="57"/>
        <source>Copyright © </source>
        <translatorcomment>Copyright ©</translatorcomment>
        <translation>Copyright © </translation>
    </message>
    <message>
        <location filename="../about.cpp" line="57"/>
        <source>License: </source>
        <translation>Licenza: </translation>
    </message>
    <message>
        <location filename="../about.cpp" line="58"/>
        <source>is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.</source>
        <translation>viene distribuito nella speranza che possa essere utile, ma SENZA ALCUNA GARANZIA; senza nemmeno la garanzia implicita di COMMERCIABILITÀ o IDONEITÀ PER UNO SCOPO PARTICOLARE.</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="58"/>
        <source>See the GNU General Public License version 3.0 for more details.</source>
        <translation>Per maggiori dettagli vedi la GNU General Public License versione 3.0.</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="59"/>
        <source>Source code</source>
        <translation>Codice sorgente</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="60"/>
        <source>Website</source>
        <translation>Sito web</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="60"/>
        <source>Version history</source>
        <translation>Cronolgia versioni</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="60"/>
        <source>Created: </source>
        <translation>Creato: </translation>
    </message>
    <message>
        <location filename="../about.cpp" line="60"/>
        <source>Compiled on: </source>
        <translation>Compilato: </translation>
    </message>
    <message>
        <location filename="../about.cpp" line="60"/>
        <source>Runs on: </source>
        <translation>Esegui il: </translation>
    </message>
    <message>
        <location filename="../about.cpp" line="60"/>
        <source> is in the folder: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../about.cpp" line="67"/>
        <location filename="../about.cpp" line="95"/>
        <location filename="../about.cpp" line="97"/>
        <source>Compiler:</source>
        <translation>Compilatore:</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="75"/>
        <source>Compiler: Clang version </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../about.cpp" line="80"/>
        <source>Compiler: LLVM/Clang/LLD based MinGW version </source>
        <translation>Compilatore: MinGW (GCC per Windows) versione </translation>
    </message>
    <message>
        <location filename="../about.cpp" line="86"/>
        <source>Compiler: MinGW (GCC for Windows) version </source>
        <translation>Compilatore: MinGW (GCC per Windows) versione </translation>
    </message>
    <message>
        <location filename="../about.cpp" line="102"/>
        <location filename="../about.cpp" line="104"/>
        <location filename="../about.cpp" line="106"/>
        <location filename="../about.cpp" line="108"/>
        <location filename="../about.cpp" line="110"/>
        <source>Programming language: C++</source>
        <translation>Linguaggio programmazione: C++</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="102"/>
        <source>C++ version: C++20</source>
        <translation>Versione C++: 20</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="104"/>
        <source>C++ version: C++17</source>
        <translation>Versione C++: 17</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="106"/>
        <source>C++ version: C++14</source>
        <translation>Versione C++: 14</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="108"/>
        <source>C++ version: C++11</source>
        <translation>Versione C++: 11</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="110"/>
        <source>C++ version: Unknown</source>
        <translatorcomment>C++ version: Unknown</translatorcomment>
        <translation>Versione C++: sconosciuta</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="112"/>
        <source>Application framework: Qt version </source>
        <translation>Framework applicazione: Qt versione </translation>
    </message>
    <message>
        <location filename="../about.cpp" line="114"/>
        <source>Processor architecture: 32-bit</source>
        <translation>Architettura processore: 32 bit</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="117"/>
        <source>Processor architecture: 64-bit</source>
        <translation>Architettura processore: 64 bit</translation>
    </message>
</context>
<context>
    <name>Dialog</name>
    <message>
        <location filename="../dialog.ui" line="37"/>
        <source>New NSIS File list</source>
        <translation>Nuovo elenco file NSIS</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="77"/>
        <source>Exit</source>
        <translation>Esci</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="88"/>
        <source>Right click here</source>
        <translation>Fai clic destro
mouse qui</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="104"/>
        <source>x</source>
        <translation>x</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="56"/>
        <location filename="../dialog.cpp" line="521"/>
        <location filename="../dialog.cpp" line="690"/>
        <location filename="../dialog.cpp" line="706"/>
        <source>Paste to Clipboard</source>
        <translation>Incolla negli Appunti</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="59"/>
        <location filename="../dialog.cpp" line="93"/>
        <location filename="../dialog.cpp" line="97"/>
        <location filename="../dialog.cpp" line="526"/>
        <location filename="../dialog.cpp" line="693"/>
        <location filename="../dialog.cpp" line="709"/>
        <source>Save File list</source>
        <translation>Salva elenco file</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="74"/>
        <source>About</source>
        <translation>Info programma</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="78"/>
        <source>About Qt</source>
        <translation>Info Qt</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="84"/>
        <source>Paste to Clipboard (Do not save to file)</source>
        <translation>Incolla negli Appunti (non salvare nel file)</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="103"/>
        <source>Open &quot;Save File list&quot; automatically</source>
        <translation>Apri automaticamente &quot;Salva elenco file&quot;</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="114"/>
        <source>Window always on top</source>
        <translation>Finestra sempre in primo piano</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="126"/>
        <source>English</source>
        <translation>Inglese</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="130"/>
        <source>Italian</source>
        <translation>Italiano</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="134"/>
        <source>Swedish</source>
        <translation>Svedese</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="138"/>
        <source>Select language file...</source>
        <translation>Seleziona file lingua...</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="141"/>
        <source>Check for updates</source>
        <translation>Controlla aggiornamenti</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="146"/>
        <source>Check for updates at startup</source>
        <translation>Controlla aggiornamenti all&apos;avvio</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="161"/>
        <source>Update</source>
        <translation>Aggiorna</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="165"/>
        <source>Delete all settings and exit</source>
        <translation>Elimina tutte le impostazioni ed esci</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="170"/>
        <source>Uninstall</source>
        <translation>Disinstalla</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="177"/>
        <source>Desktop Shortcut</source>
        <translation>Collegamento sul desktop</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="204"/>
        <source>Applications menu Shortcut</source>
        <translation>Collegamento menu applicazioni</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="219"/>
        <source>Light Theme</source>
        <translation>Tema chiaro</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="221"/>
        <source>Dark Theme</source>
        <translation>Tema scuro</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="272"/>
        <source>The program is intended for creating file lists for NSIS installer program.</source>
        <translation>Il programma è destinato alla creazione di elenchi di file per il programma di installazione di NSIS.</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="273"/>
        <source>Many thanks to bovirus for the Italian translation. And for many good ideas that have made the program better.</source>
        <translation>Molte grazie a bovirus per la traduzioen italiana. E per molte buone idee per rendere il programma migliore.</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="429"/>
        <source>Open</source>
        <translation>Apri</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="431"/>
        <source>Open language file</source>
        <translation>Apri file lingua</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="436"/>
        <source>Language files (*.qm)</source>
        <translation>File lingua (*.qm)</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="586"/>
        <source>All the program&apos;s settings files will be deleted.&lt;br&gt;Do you want to continue?</source>
        <translation>Tutti i file delle impostazioni del programma verranno eliminati.&lt;br&gt;Vuoi continuare?</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="598"/>
        <source>Do you want to remove all the program&apos;s&lt;br&gt;settings files before uninstalling?</source>
        <translation>Vuoi rimuovere tutti i file delle impostazioni del&lt;br&gt;programma prima della disinstallazione?</translation>
    </message>
    <message>
        <location filename="../check_for_updates.cpp" line="23"/>
        <location filename="../check_for_updates.cpp" line="34"/>
        <source>Please click on &quot;Update&quot;</source>
        <translation>Seleziona &quot;Aggiorna&quot;</translation>
    </message>
    <message>
        <location filename="../createlist.cpp" line="50"/>
        <source>Select files in base directory</source>
        <translation>Seleziona i file nella cartella base</translation>
    </message>
    <message>
        <location filename="../createlist.cpp" line="52"/>
        <source>Select Files</source>
        <translation>Seleziona i file</translation>
    </message>
    <message>
        <location filename="../createlist.cpp" line="53"/>
        <location filename="../createlist.cpp" line="153"/>
        <location filename="../dialog.cpp" line="430"/>
        <location filename="../save.cpp" line="48"/>
        <location filename="../save.cpp" line="65"/>
        <source>Cancel</source>
        <translation>Annulla</translation>
    </message>
    <message>
        <location filename="../createlist.cpp" line="68"/>
        <location filename="../createlist.cpp" line="116"/>
        <source>ERROR 1: An unexpected error occurred.&lt;br&gt;Could not open a temporary file.</source>
        <translation>ERRORE 1: si è verificato un errore imprevisto.&lt;br&gt;Impossibile aprire un file temporaneo.</translation>
    </message>
    <message>
        <location filename="../createlist.cpp" line="69"/>
        <location filename="../createlist.cpp" line="117"/>
        <location filename="../createlist.cpp" line="259"/>
        <location filename="../createlist.cpp" line="274"/>
        <location filename="../createlist.cpp" line="401"/>
        <location filename="../createlist.cpp" line="410"/>
        <location filename="../deletesettings.cpp" line="58"/>
        <location filename="../save.cpp" line="47"/>
        <location filename="../save.cpp" line="80"/>
        <location filename="../save.cpp" line="99"/>
        <location filename="../save.cpp" line="116"/>
        <source>Ok</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../createlist.cpp" line="86"/>
        <location filename="../createlist.cpp" line="132"/>
        <location filename="../createlist.cpp" line="294"/>
        <source>Created by</source>
        <translation>Creato da</translation>
    </message>
    <message>
        <location filename="../createlist.cpp" line="133"/>
        <location filename="../createlist.cpp" line="295"/>
        <source>License</source>
        <translation>Licenza</translation>
    </message>
    <message>
        <location filename="../createlist.cpp" line="151"/>
        <source>Select folders. Files in the folders will be included.</source>
        <translation>Seleziona le cartelle. I file nelle cartelle saranno inclusi.</translation>
    </message>
    <message>
        <location filename="../createlist.cpp" line="152"/>
        <source>Select Folders</source>
        <translation>Seleziona cartelle</translation>
    </message>
    <message>
        <location filename="../createlist.cpp" line="258"/>
        <source>ERROR 2: An unexpected error occurred.&lt;br&gt;Could not open a temporary file.</source>
        <translation>ERRORE 2: si è verificato un errore imprevisto.&lt;br&gt;Impossibile aprire un file temporaneo.</translation>
    </message>
    <message>
        <location filename="../createlist.cpp" line="273"/>
        <source>Could not paste data to clipboard.&lt;br&gt;You must paste paths to folders and files.&lt;br&gt;(When you drag and drop folders and files,&lt;br&gt;the paths are pasted.)</source>
        <translation>Impossibile incollare i dati negli Appunti.&lt;br&gt;Devi incollare i percorsi di cartelle e file.&lt;br&gt;(quando trascini e rilasci cartelle e file,&lt;br&gt;i percorsi vengono incollati)</translation>
    </message>
    <message>
        <location filename="../createlist.cpp" line="400"/>
        <source>Successfully pasted to clipboard.</source>
        <translation>Incollato correttamente negli Appunti.</translation>
    </message>
    <message>
        <location filename="../createlist.cpp" line="409"/>
        <source>Failed pasted to clipboard.</source>
        <translation>Impossibile incollare negli Appunti.</translation>
    </message>
    <message>
        <source>Select directories in any place. (Files in subdirectories will also be included.)</source>
        <translation type="vanished">Seleziona le cartelle in qualsiasi percorso (saranno inclusi anche i file nelle sottocartelle)</translation>
    </message>
    <message>
        <location filename="../deletesettings.cpp" line="45"/>
        <source>Yes</source>
        <translation>Sì</translation>
    </message>
    <message>
        <location filename="../deletesettings.cpp" line="46"/>
        <source>No</source>
        <translation>No</translation>
    </message>
    <message>
        <location filename="../deletesettings.cpp" line="56"/>
        <source>Failed to delete the program&apos;s settings files.&lt;br&gt;Check your file permissions.</source>
        <translation>Impossibile eliminare i file delle impostazioni del programma.&lt;br&gt;Controlla i tuoi permessi sui file.</translation>
    </message>
    <message>
        <location filename="../save.cpp" line="45"/>
        <source>Save NSIS File list</source>
        <translation>Salva elenco file NSIS</translation>
    </message>
    <message>
        <location filename="../save.cpp" line="63"/>
        <source>The file exists.&lt;br&gt;Do you want to overwrite the old file with the new file?</source>
        <translation>Il file esiste.&lt;br&gt;Vuoi sovrascrivere il vecchio file con il nuovo file?</translation>
    </message>
    <message>
        <location filename="../save.cpp" line="64"/>
        <source>Overwrite</source>
        <translation>Sorascrivi</translation>
    </message>
    <message>
        <location filename="../save.cpp" line="79"/>
        <location filename="../save.cpp" line="98"/>
        <source>Could not save the file.&lt;br&gt;Check your file permissions.</source>
        <translation>Impossibile salvare il file.&lt;br&gt;Controlla i tuoi permessi sui file.</translation>
    </message>
    <message>
        <location filename="../save.cpp" line="115"/>
        <source>ERROR 3: An unexpected error occurred.&lt;br&gt;Could not save the file.</source>
        <translation>ERRORE 3: si è verificato un errore imprevisto.&lt;br&gt;Impossibile salvare il file.</translation>
    </message>
</context>
</TS>
