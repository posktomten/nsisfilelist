<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1">
<context>
    <name>CheckUpdate</name>
    <message>
        <location filename="../checkupdate.cpp" line="74"/>
        <location filename="../checkupdate.cpp" line="96"/>
        <location filename="../checkupdate.cpp" line="176"/>
        <location filename="../checkupdate.cpp" line="296"/>
        <source>No Internet connection was found.
Please check your Internet settings and firewall.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../checkupdate.cpp" line="39"/>
        <location filename="../checkupdate.cpp" line="53"/>
        <location filename="../checkupdate.cpp" line="71"/>
        <location filename="../checkupdate.cpp" line="97"/>
        <location filename="../checkupdate.cpp" line="116"/>
        <location filename="../checkupdate.cpp" line="126"/>
        <location filename="../checkupdate.cpp" line="137"/>
        <location filename="../checkupdate.cpp" line="173"/>
        <location filename="../checkupdate.cpp" line="210"/>
        <location filename="../checkupdate.cpp" line="286"/>
        <location filename="../checkupdate.cpp" line="297"/>
        <source>Ok</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../checkupdate.cpp" line="114"/>
        <source>Your version of </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../checkupdate.cpp" line="115"/>
        <source> is newer than the latest official version. </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../checkupdate.cpp" line="125"/>
        <source>You have the latest version of </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../checkupdate.cpp" line="136"/>
        <source>There was an error when the version was checked.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../checkupdate.cpp" line="209"/>
        <source>
There was an error when the version was checked.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../checkupdate.cpp" line="256"/>
        <source>Updates:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../checkupdate.cpp" line="282"/>
        <source>There is a new version of </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../checkupdate.cpp" line="284"/>
        <source>Latest version: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="29"/>
        <source>No error.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="33"/>
        <source>The remote server closed the connection prematurely, before the entire reply was received and processed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="37"/>
        <source>The remote host name was not found (invalid hostname).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="41"/>
        <source>The connection to the remote server timed out.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="45"/>
        <source>The operation was canceled via calls to abort() or close() before it was finished.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="49"/>
        <source>The SSL/TLS handshake failed and the encrypted channel could not be established. The sslErrors() signal should have been emitted.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="53"/>
        <source>The connection was broken due to disconnection from the network, however the system has initiated roaming to another access point. The request should be resubmitted and will be processed as soon as the connection is re-established.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="57"/>
        <source>The connection was broken due to disconnection from the network or failure to start the network.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="61"/>
        <source>The background request is not currently allowed due to platform policy.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="65"/>
        <source>While following redirects, the maximum limit was reached. The limit is by default set to 50 or as set by QNetworkRequest::setMaxRedirectsAllowed().</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="69"/>
        <source>While following redirects, the network access API detected a redirect from a encrypted protocol (https) to an unencrypted one (http).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="73"/>
        <source>An unknown network-related error was detected.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="77"/>
        <source>The connection to the proxy server was refused (the proxy server is not accepting requests).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="81"/>
        <source>The proxy server closed the connection prematurely, before the entire reply was received and processed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="85"/>
        <source>The proxy host name was not found (invalid proxy hostname).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="89"/>
        <source>The connection to the proxy timed out or the proxy did not reply in time to the request sent.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="93"/>
        <source>The proxy requires authentication in order to honour the request but did not accept any credentials offered (if any).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="97"/>
        <source>An unknown proxy-related error was detected.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="101"/>
        <source>The access to the remote content was denied (similar to HTTP error 403).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="105"/>
        <source>The operation requested on the remote content is not permitted.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="109"/>
        <source>The remote content was not found at the server (similar to HTTP error 404).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="113"/>
        <source>The remote server requires authentication to serve the content but the credentials provided were not accepted (if any).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="117"/>
        <source>The request needed to be sent again, but this failed for example because the upload data could not be read a second time.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="121"/>
        <source>The request could not be completed due to a conflict with the current state of the resource.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="125"/>
        <source>The requested resource is no longer available at the server.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="129"/>
        <source>An unknown error related to the remote content was detected.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="133"/>
        <source>The Network Access API cannot honor the request because the protocol is not known.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="137"/>
        <source>The requested operation is invalid for this protocol.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="141"/>
        <source>A breakdown in protocol was detected (parsing error, invalid or unexpected responses, etc.).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="145"/>
        <source>The server encountered an unexpected condition which prevented it from fulfilling the request.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="149"/>
        <source>The server does not support the functionality required to fulfill the request.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="153"/>
        <source>The server is unable to handle the request at this time.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="157"/>
        <source>Unknown Server Error.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="161"/>
        <source>The remote server refused the connection (the server is not accepting requests).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="165"/>
        <source>Unknown Error.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Createshortcut</name>
    <message>
        <location filename="../createshortcut.cpp" line="36"/>
        <location filename="../createshortcut.cpp" line="52"/>
        <source>Shortcut could not be created in</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../createshortcut.cpp" line="37"/>
        <location filename="../createshortcut.cpp" line="53"/>
        <location filename="../removeshortcut.cpp" line="34"/>
        <location filename="../removeshortcut.cpp" line="48"/>
        <source>Ok</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../removeshortcut.cpp" line="33"/>
        <location filename="../removeshortcut.cpp" line="47"/>
        <source>The shortcut could not be removed:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DownloadInstall</name>
    <message>
        <location filename="../download_install.ui" line="20"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../download_install.ui" line="54"/>
        <source>Downloading...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../download_install.ui" line="70"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../download_install.ui" line="86"/>
        <source>Install</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../download_install.cpp" line="42"/>
        <source>Download </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../download_install.cpp" line="57"/>
        <location filename="../download_install.cpp" line="70"/>
        <source>Ok</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../download_install.cpp" line="90"/>
        <source>The download is complete.&lt;br&gt;The current version will be uninstalled before the new one is installed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="28"/>
        <source>No error.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="32"/>
        <source>The remote server closed the connection prematurely, before the entire reply was received and processed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="36"/>
        <source>The remote host name was not found (invalid hostname).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="40"/>
        <source>The connection to the remote server timed out.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="44"/>
        <source>The operation was canceled via calls to abort() or close() before it was finished.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="48"/>
        <source>The SSL/TLS handshake failed and the encrypted channel could not be established. The sslErrors() signal should have been emitted.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="52"/>
        <source>The connection was broken due to disconnection from the network, however the system has initiated roaming to another access point. The request should be resubmitted and will be processed as soon as the connection is re-established.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="56"/>
        <source>The connection was broken due to disconnection from the network or failure to start the network.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="60"/>
        <source>The background request is not currently allowed due to platform policy.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="64"/>
        <source>While following redirects, the maximum limit was reached. The limit is by default set to 50 or as set by QNetworkRequest::setMaxRedirectsAllowed().</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="68"/>
        <source>While following redirects, the network access API detected a redirect from a encrypted protocol (https) to an unencrypted one (http).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="72"/>
        <source>An unknown network-related error was detected.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="76"/>
        <source>The connection to the proxy server was refused (the proxy server is not accepting requests).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="80"/>
        <source>The proxy server closed the connection prematurely, before the entire reply was received and processed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="84"/>
        <source>The proxy host name was not found (invalid proxy hostname).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="88"/>
        <source>The connection to the proxy timed out or the proxy did not reply in time to the request sent.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="92"/>
        <source>The proxy requires authentication in order to honour the request but did not accept any credentials offered (if any).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="96"/>
        <source>An unknown proxy-related error was detected.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="100"/>
        <source>The access to the remote content was denied (similar to HTTP error 403).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="104"/>
        <source>The operation requested on the remote content is not permitted.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="108"/>
        <source>The remote content was not found at the server (similar to HTTP error 404).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="112"/>
        <source>The remote server requires authentication to serve the content but the credentials provided were not accepted (if any).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="116"/>
        <source>The request needed to be sent again, but this failed for example because the upload data could not be read a second time.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="120"/>
        <source>The request could not be completed due to a conflict with the current state of the resource.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="124"/>
        <source>The requested resource is no longer available at the server.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="128"/>
        <source>An unknown error related to the remote content was detected.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="132"/>
        <source>The Network Access API cannot honor the request because the protocol is not known.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="136"/>
        <source>The requested operation is invalid for this protocol.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="140"/>
        <source>A breakdown in protocol was detected (parsing error, invalid or unexpected responses, etc.).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="144"/>
        <source>The server encountered an unexpected condition which prevented it from fulfilling the request.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="148"/>
        <source>The server does not support the functionality required to fulfill the request.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="152"/>
        <source>The server is unable to handle the request at this time.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="156"/>
        <source>Unknown Server Error.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="160"/>
        <source>The remote server refused the connection (the server is not accepting requests).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="164"/>
        <source>Unknown Error.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Dialog</name>
    <message>
        <location filename="../dialog.ui" line="37"/>
        <source>New NSIS File list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="77"/>
        <source>Exit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="88"/>
        <source>Right click here</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="104"/>
        <source>x</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="56"/>
        <location filename="../dialog.cpp" line="521"/>
        <location filename="../dialog.cpp" line="690"/>
        <location filename="../dialog.cpp" line="706"/>
        <source>Paste to Clipboard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="59"/>
        <location filename="../dialog.cpp" line="93"/>
        <location filename="../dialog.cpp" line="97"/>
        <location filename="../dialog.cpp" line="526"/>
        <location filename="../dialog.cpp" line="693"/>
        <location filename="../dialog.cpp" line="709"/>
        <source>Save File list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="74"/>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="78"/>
        <source>About Qt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="84"/>
        <source>Paste to Clipboard (Do not save to file)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="103"/>
        <source>Open &quot;Save File list&quot; automatically</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="114"/>
        <source>Window always on top</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="126"/>
        <source>English</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="130"/>
        <source>Italian</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="134"/>
        <source>Swedish</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="138"/>
        <source>Select language file...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="141"/>
        <source>Check for updates</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="146"/>
        <source>Check for updates at startup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="161"/>
        <source>Update</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="165"/>
        <source>Delete all settings and exit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="170"/>
        <source>Uninstall</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="177"/>
        <source>Desktop Shortcut</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="204"/>
        <source>Applications menu Shortcut</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="219"/>
        <source>Light Theme</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="221"/>
        <source>Dark Theme</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="272"/>
        <source>The program is intended for creating file lists for NSIS installer program.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="273"/>
        <source>Many thanks to bovirus for the Italian translation. And for many good ideas that have made the program better.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="429"/>
        <source>Open</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="431"/>
        <source>Open language file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="436"/>
        <source>Language files (*.qm)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="586"/>
        <source>All the program&apos;s settings files will be deleted.&lt;br&gt;Do you want to continue?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="598"/>
        <source>Do you want to remove all the program&apos;s&lt;br&gt;settings files before uninstalling?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../check_for_updates.cpp" line="23"/>
        <location filename="../check_for_updates.cpp" line="34"/>
        <source>Please click on &quot;Update&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../createlist.cpp" line="50"/>
        <source>Select files in base directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../createlist.cpp" line="52"/>
        <source>Select Files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../createlist.cpp" line="53"/>
        <location filename="../createlist.cpp" line="153"/>
        <location filename="../dialog.cpp" line="430"/>
        <location filename="../save.cpp" line="48"/>
        <location filename="../save.cpp" line="65"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../createlist.cpp" line="68"/>
        <location filename="../createlist.cpp" line="116"/>
        <source>ERROR 1: An unexpected error occurred.&lt;br&gt;Could not open a temporary file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../createlist.cpp" line="69"/>
        <location filename="../createlist.cpp" line="117"/>
        <location filename="../createlist.cpp" line="259"/>
        <location filename="../createlist.cpp" line="274"/>
        <location filename="../createlist.cpp" line="401"/>
        <location filename="../createlist.cpp" line="410"/>
        <location filename="../deletesettings.cpp" line="58"/>
        <location filename="../save.cpp" line="47"/>
        <location filename="../save.cpp" line="80"/>
        <location filename="../save.cpp" line="99"/>
        <location filename="../save.cpp" line="116"/>
        <source>Ok</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../createlist.cpp" line="86"/>
        <location filename="../createlist.cpp" line="132"/>
        <location filename="../createlist.cpp" line="294"/>
        <source>Created by</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../createlist.cpp" line="133"/>
        <location filename="../createlist.cpp" line="295"/>
        <source>License</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../createlist.cpp" line="151"/>
        <source>Select folders. Files in the folders will be included.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../createlist.cpp" line="152"/>
        <source>Select Folders</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../createlist.cpp" line="258"/>
        <source>ERROR 2: An unexpected error occurred.&lt;br&gt;Could not open a temporary file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../createlist.cpp" line="273"/>
        <source>Could not paste data to clipboard.&lt;br&gt;You must paste paths to folders and files.&lt;br&gt;(When you drag and drop folders and files,&lt;br&gt;the paths are pasted.)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../createlist.cpp" line="400"/>
        <source>Successfully pasted to clipboard.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../createlist.cpp" line="409"/>
        <source>Failed pasted to clipboard.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../deletesettings.cpp" line="45"/>
        <source>Yes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../deletesettings.cpp" line="46"/>
        <source>No</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../deletesettings.cpp" line="56"/>
        <source>Failed to delete the program&apos;s settings files.&lt;br&gt;Check your file permissions.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../save.cpp" line="45"/>
        <source>Save NSIS File list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../save.cpp" line="63"/>
        <source>The file exists.&lt;br&gt;Do you want to overwrite the old file with the new file?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../save.cpp" line="64"/>
        <source>Overwrite</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../save.cpp" line="79"/>
        <location filename="../save.cpp" line="98"/>
        <source>Could not save the file.&lt;br&gt;Check your file permissions.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../save.cpp" line="115"/>
        <source>ERROR 3: An unexpected error occurred.&lt;br&gt;Could not save the file.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>About</name>
    <message>
        <location filename="../about.cpp" line="56"/>
        <source>About </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../about.cpp" line="57"/>
        <source>Copyright © </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../about.cpp" line="57"/>
        <source>License: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../about.cpp" line="58"/>
        <source>is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../about.cpp" line="58"/>
        <source>See the GNU General Public License version 3.0 for more details.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../about.cpp" line="59"/>
        <source>Source code</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../about.cpp" line="60"/>
        <source>Website</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../about.cpp" line="60"/>
        <source>Version history</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../about.cpp" line="60"/>
        <source>Created: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../about.cpp" line="60"/>
        <source>Compiled on: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../about.cpp" line="60"/>
        <source>Runs on: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../about.cpp" line="60"/>
        <source> is in the folder: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../about.cpp" line="67"/>
        <location filename="../about.cpp" line="95"/>
        <location filename="../about.cpp" line="97"/>
        <source>Compiler:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../about.cpp" line="75"/>
        <source>Compiler: Clang version </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../about.cpp" line="80"/>
        <source>Compiler: LLVM/Clang/LLD based MinGW version </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../about.cpp" line="86"/>
        <source>Compiler: MinGW (GCC for Windows) version </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../about.cpp" line="102"/>
        <location filename="../about.cpp" line="104"/>
        <location filename="../about.cpp" line="106"/>
        <location filename="../about.cpp" line="108"/>
        <location filename="../about.cpp" line="110"/>
        <source>Programming language: C++</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../about.cpp" line="102"/>
        <source>C++ version: C++20</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../about.cpp" line="104"/>
        <source>C++ version: C++17</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../about.cpp" line="106"/>
        <source>C++ version: C++14</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../about.cpp" line="108"/>
        <source>C++ version: C++11</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../about.cpp" line="110"/>
        <source>C++ version: Unknown</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../about.cpp" line="112"/>
        <source>Application framework: Qt version </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../about.cpp" line="114"/>
        <source>Processor architecture: 32-bit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../about.cpp" line="117"/>
        <source>Processor architecture: 64-bit</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
