
Qt 6.8.2 dell MinGW LLVM 
SET PATH=C:\Qt\Tools\llvm-mingw1706_64\bin;C:\Qt\6.8.2\llvm-mingw_64\bin;%PATH%
C:\Qt\6.8.2\llvm-mingw_64\bin\windeployqt6.exe nsisfilelist.exe -no-translations  -verbose 1 --compiler-runtime

Qt 6.8.2 MinGW
SET PATH=C:\Qt\6.8.2\mingw_64\bin;C:\Qt\Tools\mingw1310_64\bin;%PATH%
C:\Qt\6.8.2\mingw_64\bin\windeployqt6.exe nsisfilelist.exe -no-translations  -verbose 1 --compiler-runtime

MSVC 64 Qt6.8.2
SET PATH=C:\Qt\6.8.2\msvc2022_64\bin;%PATH%
C:\Qt\6.8.2\msvc2022_64\bin\windeployqt6.exe nsisfilelist.exe -no-translations  -verbose 1 --compiler-runtime
